/*
 * Project: Bomberman
 * Author: mc & md
 * File:   maptest.hpp
 */

#ifndef BM_MAPTEST_H
#define BM_MAPTEST_H

#include <iostream>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "mapabs.hpp"

#include "factoryfielddestructible.hpp"
#include "factoryfieldindestructible.hpp"

/**
 * \class MapTest
 *
 * \brief This is the map for test
 *
 * I - INDESTRUCTIBLE
 * E - DESTRUCTIBLE EMPTY
 * B - DESTRUCTIBLE BARRICADE
 *
 *   0 1 2 3 4 5 6 7 8 9 10
 * 0 I I I I I I I I I I I
 * 1 I E E B B B E B E B I
 * 2 I E I B I E I B I E I
 * 3 I E E B B E E E B E I
 * 4 I B I E I E I B I E I
 * 5 I E E E E B B E B E I
 * 6 I I I I I I I I I I I
 *
 */

class MapTest : public MapAbs {
public:
    /**
     * This is constructor of MapTest.
     *
    */
    MapTest(bool is_additional_bomberman = false);
    ~MapTest();

  private:

      typedef std::vector<MapPosition> VMapPosition;
      typedef std::vector<std::vector<FieldAbs::FieldData>> VVFieldData;
};

#endif /* BM_MAPTEST_H */

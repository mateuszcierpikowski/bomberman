/*
 * Project: Bomberman
 * Author: mc & md
 * File:   factoryfielddestructible.hpp
 */

#ifndef BM_FACTORYFIELDDESTRUCTIBLE_H
#define BM_FACTORYFIELDDESTRUCTIBLE_H

#include "factoryfieldabs.hpp"

#include "fielddestructible.hpp"

#include "factoryaddition.hpp"

#include "additionpositiveextrabomb.hpp"
#include "additionpositiveextrarange.hpp"
#include "additionnegativeinvertmode.hpp"

/**
 * \class FactoryFieldDestructible
 *
 * \brief This is the factory, which create destructible fields
 *
*/
class FactoryFieldDestructible : public FactoryFieldAbs {
public:
    FactoryFieldDestructible();
    ~FactoryFieldDestructible();

    /**
    * This method creates specific \p type destructible field and returns a shared pointer to it.
    *
    */
    virtual PField createField(FieldAbs::FieldData type);

    static const double TIME_OF_INVERT_STEERING_MODE;

private:

};

#endif /* BM_FACTORYFIELDDESTRUCTIBLE_H */

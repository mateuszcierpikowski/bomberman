/*
 * Project: Bomberman
 * Author: mc & md
 * File:   fieldindestructible.hpp
 */

#ifndef BM_FIELDINDESTRUCTIBLE_H
#define BM_FIELDINDESTRUCTIBLE_H

#include "fieldabs.hpp"

/**
 * \class FieldIndestructible
 *
 * \brief This is the indestructible field
 *
*/


class FieldIndestructible : public FieldAbs {
public:
    FieldIndestructible(FieldData field_data);
    ~FieldIndestructible();

    virtual void addBomberman(const PBomberman & bomberman);
    virtual void deleteBomberman(const PBomberman & bomberman);

    virtual void addBomb(const PBomb & bomb);
    virtual bool startBombExplosion(const PBomb & bomb, PBomb & bomb_chain_reaction);
    virtual bool stopBombExplosion(const PBomb & bomb);

};

#endif /* BM_FIELDINDESTRUCTIBLE_H */

/*
 * Project: Bomberman
 * Author: mc & md
 * File:   mapnormal.hpp
 */

#ifndef BM_MAPNORMAL_H
#define BM_MAPNORMAL_H

#include <iostream>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "mapabs.hpp"

#include "factoryfielddestructible.hpp"
#include "factoryfieldindestructible.hpp"

/**
 * \class MapNormal
 *
 * \brief This is the standard map
 *
*/

class MapNormal : public MapAbs {
public:
    /**
     * This is constructor of MapNormal.
     * \param width width of the map
     * \param height height of the map
     * \param field_radius radius of the field
     * \param number_of_positive_addition number of positive addition in the map
     * \param number_of_negative_addition number of negative addition in the map
     * \param percent_of_barricaded_field percentage of barricaded fields in the map
     *
    */
    MapNormal(unsigned width, unsigned height, unsigned field_radius, unsigned number_of_positive_addition,
        unsigned number_of_negative_addition, unsigned percent_of_barricaded_field, double bomb_count_down_duration, double bomb_exlosion_duration);
    ~MapNormal();

private:

    typedef std::vector<MapPosition> VMapPosition;
    typedef std::vector<std::vector<FieldAbs::FieldData>> VVFieldData;

    /**
     * number of positive additions
    */
    unsigned numberOfPositiveAddition_;

    /**
     * number of negative additions
    */
    unsigned numberOfNegativeAddition_;

    /**
     * percentage of barricaded fields in the map
    */
    unsigned percentOfBarricadedField_;

    /**
     * number of positive addition in the map
    */
    const int NUMBER_OF_POSITIVE_ADDITION = 2;

    boost::random::mt19937 generator_;

    /**
     * This method draws the position of negative additions from \p fields_position and changing properly \p table_fields_type.
     *
     * \param number_of_negative_addition number of negative addition in the map
     * \param fields_position vector of available positions in the map
     * \param table_fields_type table of field type, which represents creating map
    */
    void drawNegativeAddition(int number_of_negative_addition, VMapPosition& fields_position, VVFieldData& table_fields_type);

    /**
     * This method draws the position of positive additions from \p fields_position and changing properly \p table_fields_type.
     *
     * \param number_of_positive_addition number of positive addition in the map
     * \param fields_position vector of available positions in the map
     * \param table_fields_type table of field type, which represents creating map
    */
    void drawPositiveAddition(int number_of_positive_addition, VMapPosition& fields_position, VVFieldData& table_fields_type);

    /**
     * This method draws the position of barricaded field from \p fields_position and changing properly \p table_fields_type.
     *
     * \param number_of_barricaded_field number of barricaded field in the map
     * \param fields_position vector of available positions in the map
     * \param table_fields_type table of field type, which represents creating map
    */
    void drawBarricadedPosition(int number_of_barricaded_field, VMapPosition& fields_position, VVFieldData& table_fields_type);

    /**
     * This method create vector of the field \p fieldList_ having type like in \p table_fields_type.
     *
     * \param table_fields_type table of field type, which represents creating map
    */
    void createFieldList(VVFieldData& table_fields_type);

    /**
     * This method create table of field type in the map.
     *
     * \param table_fields_type table of field type, which represents creating map
    */
    void initFieldsTypeTable(VVFieldData& table_fields_type);

    /**
     * This method create vector of available positions in the map.
     *
     * \param fields_position vector of available positions in the map
    */
    void initFieldsPosition(VMapPosition& fields_position);

    /**
     * This method changing the type of fields to destructible in \p table_fields_type, which map position is in \p fields_position.
     *
     * \param fields_position vector of available positions in the map
     * \param table_fields_type table of field type, which represents creating map
    */
    void changeAllTypeFieldsToDestructible(VMapPosition& fields_position, VVFieldData& table_fields_type);

    /**
     * This method draw the int in range <0,\p max_range>.
     *
     * \param max_range maximum range
    */
    int randomInt(int max_range);

    /**
    * This method set initial bomberman position.
    *
   */
    void initBombermansPosition();

};

#endif /* BM_MAPNORMAL_H */

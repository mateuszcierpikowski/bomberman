/*
 * Project: Bomberman
 * Author: mc & md
 * File:   factoryaddition.hpp
 */

#ifndef BM_FACTORYADDITION_H
#define BM_FACTORYADDITION_H

#include "additionabs.hpp"
#include <map>

/**
 * \class FactoryAddition
 *
 * \brief This is the scalable factory, which create additions
 *
*/

class FactoryAddition {
public:
    typedef AdditionAbs* (*CreateAddition)();

    /**
    * This method registers additions.
    *
    * \param type a type of a registered addition
    * \param addition a pointer to function, which creates an addition
    */
    void registerAddition(AdditionAbs::AdditionType type, CreateAddition additon);

    /**
    * This method creates addition and returns a shared pointer to it.
    *
    */
    PAddition create(AdditionAbs::AdditionType type);

    /**
    * This method returns an instance of a scalable addition factory.
    *
    */
    static FactoryAddition& getInstance();

private:
    FactoryAddition();
    FactoryAddition(const FactoryAddition&) = delete;
    FactoryAddition operator=(const FactoryAddition&) = delete;

    typedef std::map<AdditionAbs::AdditionType, CreateAddition> Creators;

    /**
    * map of fields type and pointers to function, which creates an addition.
    *
    */
    Creators creators_;
};

#endif /* BM_FACTORYADDITION_H */

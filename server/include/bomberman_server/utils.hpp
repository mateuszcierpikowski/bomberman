/*
* Project: Bomberman
* Author: mc & md
* File:   utils.hpp
*/

#ifndef BM_UTILS_H
#define BM_UTILS_H

/**
* \class Result
*
* \brief This enum specify a result of the Game.
*/
enum class Result {
    VICTORY = 0, DRAW = 1, DEFEAT = 2, GAME_RUN = 3
};

/**
* \struct Move
*
* \brief This struct carries data of player moves.
*/
struct Move {
    unsigned up_;
    unsigned down_;
    unsigned left_;
    unsigned right_;
    Move()
    : up_(0), down_(0), left_(0), right_(0)
    { }
    Move(unsigned up, unsigned down, unsigned left, unsigned right)
    : up_(up), down_(down), left_(left), right_(right) { }
};

/**
* \struct MapPosition
*
* \brief This struct carries data of a position in a map coordinate.
*/
struct MapPosition {
    MapPosition() {};
    MapPosition (unsigned map_x, unsigned map_y) : mapX_(map_x), mapY_(map_y) { };

    unsigned mapX_;
    unsigned mapY_;
};

/**
* \struct FieldPosition
*
* \brief This struct carries data of a position in a field coordinate.
*/
struct FieldPosition {
  FieldPosition (int field_x, int field_y) : fieldX_(field_x), fieldY_(field_y) { };

  int fieldX_;
  int fieldY_;
};

/**
* \struct Position
*
* \brief This struct connect a position in map and field coordinate.
*/
struct Position {
  Position()
    : mapPosition_(MapPosition(0,0)), fieldPosition_(FieldPosition(0,0)) { };

  Position(MapPosition map_position, FieldPosition field_position)
    : mapPosition_(map_position), fieldPosition_(field_position) { };

  MapPosition mapPosition_;
  FieldPosition fieldPosition_;
};

#endif /* BM_UTILS_H */

/*
 * Project: Bomberman
 * Author: mc & md
 * File:   bomberman.hpp
 */

#ifndef BM_BOMBERMAN_H
#define BM_BOMBERMAN_H

#include <memory>
#include <vector>
#include <boost/chrono.hpp>

#include "utils.hpp"

/**
 * \class Bomberman
 *
 * \brief This is the bomberman, which represents a player
 *
*/

class Bomberman;

typedef std::shared_ptr<Bomberman> PBomberman;
typedef std::weak_ptr<Bomberman> PWBomberman;
typedef std::vector<PBomberman> VBombermans;

class Bomberman {
public:
    /**
     * This is constructor of a Bomberman.
     * \param position a initial position of a Bomberman
     *
    */
    Bomberman(Position position);
    virtual ~Bomberman();

    /**
    * This enum specify a mode of a Bomberman steering way.
    */
    enum Mode {
        NORMAL = 1, ///< normal steering mode
        INVERT = 2 ///< invert steering mode
    };

    /**
    * This method sets a new Position of a Bomberman.
    *
    * \param position a new position of a Bomberman
    */
    void setPosition(Position position);

    /**
    * This method returns a Position of a Bomberman.
    *
    */
    Position getPosition() const;

    /**
    * This method sets a invert steering mode.
    *
    * \param duration a duration of invert steering mode
    */
    void setInvertMode(double duration);

    /**
    * This method returns a Mode of a steering mode.
    *
    */
    Mode getMode();

    /**
    * This method increases lives of Bomberman.
    *
    */
    void increaseLives();

    /**
    * This method decreases lives of a Bomberman.
    *
    */
    void decreaseLives();

    /**
    * This method returns true if any lives remained.
    */
    bool anyLiveRemain() const;

    /**
    * This method returns lives of a Bomberman.
    */
    unsigned getLives() const;

    /**
    * This method increases maximum number of a planted Bomb by Bomberman.
    *
    */
    void increaseBombsNumber();

    /**
    * This method decreases maximum number of a planted Bomb by Bomberman.
    *
    */
    void decreaseBombsNumber();

    /**
    * This method returns true if any bombs remained.
    *
    */
    bool anyBombRemain() const;

    /**
    * This method returns maximum number of a planted Bomb by Bomberman.
    *
    */
    unsigned getBombsNumber() const;

    /**
    * This method increases range of a planted Bomb by Bomberman.
    *
    */
    void increaseBombRange();

    /**
    * This method returns range of a planted Bomb by Bomberman.
    *
    */
    unsigned getBombRange() const;

    /**
    * This method returns true if any attribute has changed
    *
    */
    bool anyAttributeChange();

private:

    /**
    * a mode of a Bomberman steering way.
    *
    */
    Mode mode_;

    /**
    * a position of a Bomberman.
    *
    */
    Position position_;

    /**
    * remained lives of a Bomberman.
    *
    */
    unsigned lives_;

    /**
    * maximum number of a planted Bomb by Bomberman.
    *
    */
    unsigned bombsNumber_;

    /**
    * range of a planted Bomb by Bomberman.
    *
    */
    unsigned bombsRange_;

    /**
    * Bomberman moving speed.
    *
    */
    unsigned speed_;

    /**
    * a point in the time, which specifies end of an invert steering mode.
    *
    */
    boost::chrono::steady_clock::time_point invertEndTime_;

    /**
    * a flag, which specify if any atributes has changed
    *
    */
    bool anyAttributeChange_;
};

#endif /* BM_BOMBERMAN_H */

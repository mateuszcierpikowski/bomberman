/*
 * Project: Bomberman
 * Author: mc & md
 * File:   factoryfieldabs.hpp
 */

#ifndef BM_FACTORYFIELDABS_H
#define BM_FACTORYFIELDABS_H

#include "fieldabs.hpp"

/**
 * \class FactoryFieldAbs
 *
 * \brief This is the abstract factory, which create fields
 *
*/

class FactoryFieldAbs;

typedef std::shared_ptr<FactoryFieldAbs> PFactoryField;

class FactoryFieldAbs {
public:
    virtual ~FactoryFieldAbs() {};

    /**
    * This pure method creates specific \p type field and returns a shared pointer to it.
    *
    */
    virtual PField createField(FieldAbs::FieldData type) = 0;
};

#endif /* BM_FACTORYFIELDABS_H */

/*
* Project: Bomberman
* Author: mc & md
* File:   additionabs.hpp
*/

#ifndef BM_ADDITIONABS_H
#define BM_ADDITIONABS_H

#include <memory>

#include "bomberman.hpp"

/**
 * \class AdditionAbs
 *
 * \brief This is the abstract class of addition for Bomberman.
 *
 * This is an interface for additions.
 *
 */

class AdditionAbs;

typedef std::shared_ptr<AdditionAbs> PAddition;
typedef std::weak_ptr<AdditionAbs> PWAddition;

class AdditionAbs {
public:
    virtual ~AdditionAbs() { };

    /**
    * This enum specify an addition type.
    *
    */
    enum class AdditionType {
        NONE = 100, ///< this is none type
        EXTRA_BOMB = 101, ///< this type increase maximum number of a planted Bomb by Bomberman
        EXTRA_RANGE = 102, ///< this type increase range of a planted Bomb by Bomberman
        INVERT_MODE = 103 ///< this type temporarily invert a Bomberman steering way.
    };

    /**
    * This method takes a shared pointer to Bomberman
    * and executes a specific addition procedure for him.
    * (changing his attributes e.g. increase lives).
    *
    * \param bomberman shared pointer to Bomberman
    */
    virtual void execute (const PBomberman& bomberman) = 0;

    /**
    * This method returns a AdditionType of this addition.
    *
    */
    virtual AdditionType getType() = 0;

};

#endif /* BM_ADDITIONABS_H */

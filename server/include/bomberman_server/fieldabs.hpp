/*
* Project: Bomberman
* Author: mc & md
* File:   fieldabs.hpp
*/

#ifndef BM_FIELDABS_H
#define BM_FIELDABS_H

#include <memory>
#include <vector>

#include "bomberman.hpp"
#include "bomb.hpp"
#include "additionabs.hpp"
#include "utils.hpp"

/**
 * \class FieldAbs
 *
 * \brief This is the abstract class of field
 *
*/

class FieldAbs;

typedef std::shared_ptr<FieldAbs> PField;
typedef std::weak_ptr<FieldAbs> PWField;
typedef std::vector<PField> VFields;
typedef std::vector<VFields> VVFields;

class FieldAbs {
public:

    /**
     * This enum specify the type of field
    */
    enum class Type {
        INDESTRUCTIBLE = 1,
        DESTRUCTIBLE = 2
    };

    /**
     * This enum specify the subtype of field
    */
    enum class SubType {
        EMPTY = 10,
        BARRICADE = 11,
        EXPLOSION = 12,
        PLANTED = 13
    };

    /**
     * \brief This struct carries data about field position in the map coordinate and specifies information of this field
    */
    struct FieldData {
        FieldData() {}
        FieldData(MapPosition map_position, Type type, SubType sub_type, AdditionAbs::AdditionType addition_type)
                : mapPosition_(map_position), type_(type), subType_(sub_type), additionType_(addition_type) { }

        void changeType(Type type) {

            type_ = type;
        }

        void changeSubType(SubType sub_type) {

            subType_ = sub_type;
        }

        void deleteAddition() {

            additionType_ = AdditionAbs::AdditionType::NONE;
        }

        void changeAddition(AdditionAbs::AdditionType addition_type) {

            additionType_ = addition_type;
        }

        MapPosition mapPosition_;
        Type type_;
        SubType subType_;
        AdditionAbs::AdditionType additionType_;
    };

    virtual ~FieldAbs();

    /**
     * This method returns TRUE if bomberman can move on this field
    */
    virtual bool checkMove();

    /**
     * This method adds a \p bomberman, who are on this field
    */
    virtual void addBomberman(const PBomberman & bomberman) = 0;

    /**
     * This method deletes a \p bomberman, who are on this field
    */
    virtual void deleteBomberman(const PBomberman & bomberman) = 0;

    /**
     * This method adds a \p bomb, planted on this field
    */
    virtual void addBomb(const PBomb & bomb) = 0;

    /**
     * This method starts explosion, which is triggered by \p bomb.
     * If is any bomb on this field, this method triggers chain reaction by returning pointer to planted bomb on this field.
    */
    virtual bool startBombExplosion(const PBomb & bomb, PBomb & bomb_chain_reaction) = 0;

    /**
     * This method stops a bomb explosion, which was triggered by \p bomb.
    */
    virtual bool stopBombExplosion(const PBomb & bomb) = 0;

    /**
     * This method returns TRUE if anything has changed and gives informations of this field
    */
    virtual bool getFieldData(FieldData& field_data);

    /**
     * This method returns true if is a bomb planted on this field
    */
    virtual bool isBombPlanted();

protected:

    /**
     * This is constructor of FieldAbs.
     * \param field_data specify a information of this field
     * \param addition specify a addition, which contains this field
     *
    */
    FieldAbs(FieldData field_data, PAddition addition);

    /**
    * a flag, which specify if anything has changed
    *
    */
    bool anyChange_;

    /**
     * a information of the field
    */
    FieldData fieldData_;

    /**
     * a shared pointer to the addition, which contains this field
    */
    PAddition addition_;

    /**
     * a shared pointer to the bomb, planted on this field
    */
    PWBomb bomb_;

    /**
     * a shared pointer to the bomb, detonated on this field
    */
    PWBomb bombDetonated_;

};

#endif /* BM_FIELDABS_H */

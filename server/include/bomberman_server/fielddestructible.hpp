/*
* Project: Bomberman
* Author: mc & md
* File:   fielddestructible.hpp
*/

#ifndef BM_FIELDDESTRUCTIBLE_H
#define BM_FIELDDESTRUCTIBLE_H

#include "fieldabs.hpp"

/**
 * \class FieldDestructible
 *
 * \brief This is the destructible field
 *
*/

class FieldDestructible : public FieldAbs{
public:
  FieldDestructible(FieldData field_data, PAddition addition);
  ~FieldDestructible();

  virtual void addBomberman(const PBomberman & bomberman);
  virtual void deleteBomberman(const PBomberman & bomberman);

  virtual void addBomb(const PBomb & bomb);
  virtual bool startBombExplosion(const PBomb & bomb, PBomb & bomb_chain_reaction);
  virtual bool stopBombExplosion(const PBomb & bomb);

private:
    /**
     * a vector of the bombermans, who are on this field
    */
    VBombermans bombermansList_;
  
};

#endif /* BM_FIELDDESTRUCTIBLE_H */

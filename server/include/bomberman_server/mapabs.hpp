/*
* Project: Bomberman
* Author: mc & md
* File:   mapabs.hpp
*/

#ifndef BM_MAPABS_H
#define BM_MAPABS_H

#include <memory>
#include <vector>
#include <iostream>

#include "fieldabs.hpp"
#include "bomberman.hpp"
#include "bomb.hpp"
#include "utils.hpp"

/**
 * \class MapAbs
 *
 * \brief This is the abstract map, which is responsible for game logic.
 *
 * This class aggregates all fields, controls bomberman moves, bomb explosion
 * and observes rules.
 *
*/

class MapAbs;

typedef std::shared_ptr<MapAbs> PMap;
typedef std::weak_ptr<MapAbs> PWMap;
typedef std::vector<Result> VResult;

class MapAbs {
public:
    virtual ~MapAbs();

    /**
     * This method controls bomberman moves and decides if that is possible.
     *
     * \param bomberman_id id of bomberman, who moves
     * \param move bomberman move
    */
    virtual void bombermanMove(unsigned bomberman_id, Move move);

    /**
     * This method controls planted bomb by bomberman and decides if that is possible.
     *
     * \param bomberman_id id of bomberman, who planted bomb
    */
    virtual void bombermanPlantBomb(unsigned bomberman_id);

    /**
     * This method controls bomberman lives and return the result of the game for them.
     *
     * \return vector of Result
    */
    virtual VResult checkBombermans();

    /**
     * This method controls bombs.
     *
    */
    virtual void checkBombs();

    /**
    * This method returns the vector of fields.
    *
    */
    virtual VVFields getFieldList();

    /**
    * This method returns the vector of bomberman.
    *
    */
    virtual VBombermans getBombermanList();

    /**
    * This method returns the vector of bombs;
    *
    */
    virtual VBombs getBombList();

protected:
    /**
     * This is constructor of MapAbs.
     * \param width width of the map
     * \param height height of the map
     * \param field_radius radius of the field
     *
    */
    MapAbs(unsigned width, unsigned height, unsigned field_radius, double bomb_count_down_duration, double bomb_exlosion_duration);

    /**
    * vector of fields, which form map
    *
    */
    VVFields fieldList_;

    /**
    * vector of active bomberman
    *
    */
    VBombermans bombermanList_;

    /**
    * vector of active bombs
    *
    */
    VBombs bombList_;

    /**
    * width of the map
    */
    unsigned width_;

    /**
    * height of the map
    */
    unsigned height_;

    /**
    * radius of the field
    */
    int fieldRadius_;

    /**
    * count down duration from planting to exlosion
    */
    double bombCountDownDuration_;

    /**
    * explosion duration of planted bombs
    */
    double bombExlosionDuration_;

private:

    /**
    * This enum specify a direction of propageted explosion.
    *
    */
    enum class Direction {
        UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3, NONE = 4
    };

    /**
     * This method controls starting bomb explosion by detonated this bomb and start propagated explosion.
     *
     * \param bomb shared pointer to Bomb, which exploded
     * \param direction_of_chain_reaction direction of the chain reaction explosion
    */
    void bombStartExplosion(PBomb bomb, Direction direction_of_chain_reaction = Direction::NONE);

    /**
    * This method controls stopping bomb explosion by detonated this bomb and declined explosion.
    *
    * \param bomb shared pointer to Bomb, which exploded
    */
    void bombStopExplosion(PBomb bomb);

    /**
    * This method recursively propagates explosion of the \p bomb in specific \p direction.
    *
    * \param bomb shared pointer to Bomb, which exploded
    * \param act_range actual range of the explosion
    * \param direction direction of the explosion
    */
    bool propagateExplosion(PBomb bomb, unsigned act_range, Direction direction);

    /**
    * This method recursively declines explosion of the \p bomb in specific \p direction.
    *
    * \param bomb shared pointer to Bomb, which exploded
    * \param act_range actual range of the explosion
    * \param direction direction of the explosion
    */
    bool declineExplosion(PBomb bomb, unsigned act_range, Direction direction);

};

#endif /* MAPABS_H */

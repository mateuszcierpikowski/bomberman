/*
 * Project: Bomberman
 * Author: mc & md
 * File:   bomb.hpp
 */

#ifndef BM_BOMB_H
#define BM_BOMB_H

#include <memory>
#include <vector>
#include <boost/chrono.hpp>
#include "bomberman.hpp"
#include "utils.hpp"


/**
 * \class Bomb
 *
 * \brief This is the bomb planted by Bomberman.
 *
*/

class Bomb;

typedef std::shared_ptr<Bomb> PBomb;
typedef std::weak_ptr<Bomb> PWBomb;
typedef std::vector<PBomb> VBombs;

class Bomb {
public:
    /**
     * This is constructor of Bomb.
     * \param map_position a position of a Bomb in a map coordinate
     * \param count_down_duration a duration of a Bomb count down
     * \param explosion_duration a duration of a Bomb explosion
     * \param bomberman a shared pointer to Bomberman, who planted this Bomb
     *
    */
    Bomb(MapPosition map_position, double count_down_duration, double explosion_duration, const PWBomberman& bomberman);
    virtual ~Bomb();

    /**
    * This enum specify a state of this Bomb.
    */
    enum State {
        COUNT_DOWN = 0, ///< count down state
        START_EXPLOSION = 1, ///< started explosion state
        EXPLOSION = 2, ///< explosion state
        STOP_EXPLOSION =3 ///< stoped explosion state
    };

    /**
     * This method detonated this Bomb.
     *
    */
    void dentonate();

    /**
    * This method return a State of this bomb.
    *
    */
    State getState();

    /**
    * This method return a position of this Bomb in a map coordinate.
    *
    */
    MapPosition getPosition() const;

    /**
    * This method return a range of this Bomb.
    *
    */
    unsigned getRange() const;

private:
    /**
    * a state of this Bomb.
    */
    State state_;

    /**
    * a position of this Bomb in a map coordinate.
    *
    */
    const MapPosition mapPosition_;

    /**
    * a duration of a Bomb explosion.
    *
    */
    const double explosionDuration_;

    /**
    * duration of a Bomb count down.
    *
    */
    const double countDownDuration_;

    /**
    * a point in time, which specifies start or stop explosion
    *
    */
    boost::chrono::steady_clock::time_point referenceTime_;

    /**
    * a shared pointer to Bomberman, who planted this Bomb.
    *
    */
    const PWBomberman bomberman_;
};

#endif /* BM_BOMB_H */

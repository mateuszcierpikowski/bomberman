/*
 * Project: Bomberman
 * Author: mc & md
 * File:   additionpositiveextrarange.hpp
 */

#ifndef BM_ADDITIONPOSITIVEEXTRARANGE_H
#define BM_ADDITIONPOSITIVEEXTRARANGE_H

#include "additionabs.hpp"

/**
 * \class AdditionPositiveExtraRange
 *
 * \brief This is the positive addition for Bomberman.
 *
 * This is the positive addition for Bomberman,
 * which increase range of a planted Bomb by Bomberman.
 *
 */

class AdditionPositiveExtraRange : public AdditionAbs {
public:
    AdditionPositiveExtraRange();
    virtual ~AdditionPositiveExtraRange();

    /**
    * This method takes a shared pointer to Bomberman
    * and executes a specific addition procedure for him
    * by increasing range of a planted Bomb by Bomberman.
    *
    * \param bomberman shared pointer to Bomberman
    */
    virtual void execute (const PBomberman& bomberman);
    virtual AdditionType getType();

};

#endif /* BM_ADDITIONPOSITIVEEXTRARANGE_H */

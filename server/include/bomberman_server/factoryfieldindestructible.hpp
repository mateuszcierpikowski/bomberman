/*
 * Project: Bomberman
 * Author: mc & md
 * File:   factoryfieldindestructible.hpp
 */

#ifndef BM_FACTORYFIELDINDESTRUCTIBLE_H
#define BM_FACTORYFIELDINDESTRUCTIBLE_H

#include "factoryfieldabs.hpp"

#include "fieldindestructible.hpp"

/**
 * \class FactoryFieldIndestructible
 *
 * \brief This is the factory, which create indestructible fields
 *
*/

class FactoryFieldIndestructible : public FactoryFieldAbs {
public:
    FactoryFieldIndestructible();
    ~FactoryFieldIndestructible();

    /**
    * This method creates specific \p type indestructible field and returns a shared pointer to it.
    *
    */
    virtual PField createField(FieldAbs::FieldData type);

private:

};

#endif /* BM_FACTORYFIELDINDESTRUCTIBLE_H */

/*
* Project: Bomberman
* Author: mc & md
* File:   additionnegativeinvertmode.hpp
*/

#ifndef BM_ADDITIONNEGATIVEINVERTMODE_H
#define BM_ADDITIONNEGATIVEINVERTMODE_H

#include "additionabs.hpp"

/**
 * \class AdditionNegativeInvertMode
 *
 * \brief This is the negative addition for Bomberman.
 *
 * This is the negative addition for Bomberman,
 * which temporarily invert his steering way.
 *
 */

class AdditionNegativeInvertMode : public AdditionAbs {
public:
    /**
     * This is constructor of AdditionNegativeInvertMode.
     * \param duration this is a duration of this addition.
     *
    */
    AdditionNegativeInvertMode(double duration);
    ~AdditionNegativeInvertMode();

    /**
     * This method takes a shared pointer to Bomberman
     * and executes a specific addition procedure for him
     * by temporarily inverting Bomberman steering way.
     *
     * \param bomberman shared pointer to Bomberman
    */
    virtual void execute (const PBomberman& bomberman);
    virtual AdditionType getType();

private:
    /**
     * a duration of this addition.
    */
    double duration_;
};

#endif /* BM_ADDITIONNEGATIVEINVERTMODE_H */

/*
 * Project: Bomberman
 * Author: mc & md
 * File:   additionpositiveextrabomb.hpp
 */

#ifndef BM_ADDITIONPOSITIVEEXTRABOMB_H
#define BM_ADDITIONPOSITIVEEXTRABOMB_H

#include "additionabs.hpp"

/**
 * \class AdditionPositiveExtraBomb
 *
 * \brief This is the positive addition for Bomberman.
 *
 * This is the positive addition for Bomberman,
 * which increase maximum number of a planted Bomb by Bomberman.
 *
 */

class AdditionPositiveExtraBomb : public AdditionAbs {
public:
    AdditionPositiveExtraBomb();
    ~AdditionPositiveExtraBomb();

    /**
    * This method takes a shared pointer to Bomberman
    * and executes a specific addition procedure for him
    * by increasing maximum number of a planted Bomb by Bomberman.
    *
    * \param bomberman shared pointer to Bomberman
    */
    virtual void execute (const PBomberman& bomberman);
    virtual AdditionType getType();

};

#endif /* BM_ADDITIONPOSITIVEEXTRABOMB_H */

/*
 * Project: Bomberman
 * Author: mc & md
 * File:   gamemaster.hpp
 */

#ifndef BM_GAMEMASTER_H
#define BM_GAMEMASTER_H

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>
#include <mutex>

#include <server_ws.hpp>
#include <json/json.h>

#include "mapnormal.hpp"

/**
 * \class GameMaster
 *
 * \brief This is the game controller, which is responsible for connection with clients.
 *
*/


typedef SimpleWeb::SocketServer<SimpleWeb::WS> WebSocket;
typedef WebSocket::SendStream WsSendStream;
typedef boost::system::error_code WsError;

typedef std::shared_ptr<WebSocket> PWebSocket;
typedef std::shared_ptr<WebSocket::Connection> PConnection;
typedef std::shared_ptr<WebSocket::Message> PMessage;

typedef std::map<size_t, PConnection> MConnection;
typedef std::map<size_t, unsigned> MConnectionId;

typedef std::vector<Move> VMove;
typedef std::vector<bool> VBool;

class GameMaster {
public:
    GameMaster();
    ~GameMaster();

    /**
    * This method starts a bomberman server.
    *
    */
    void start();

    /**
    * This method stops a bomberman server.
    *
    */
    void stop();

    /**
    * This method triggers the game logic loop.
    *
    */
    void trigger();


private:

    /**
    * This enum specify game states.
    *
    */
	enum class GameState {
		WAIT_FOR_ALL_PLAYERS, RUN, END
	};

    /**
    * a shared pointer to web socket server.
    *
    */
    PWebSocket server_;

    /**
    * a shared pointer to map.
    *
    */
    PMap map_;

    /**
    * a map to active web connections.
    *
    */
    MConnection connections_;

    /**
    * a map to active players id.
    *
    */
    MConnectionId connectionsId_;

    /**
    * a vector of the bomberman moves.
    *
    */
    VMove bomermansMove_;

    /**
    * a vector of the bomberman plants.
    *
    */
    VBool bomermansPlant_;

    /**
    * a mutex, which synchronizes connections.
    *
    */
    std::mutex mutexConnections_;

    /**
    * a mutex, which synchronizes clients request.
    *
    */
    std::mutex mutexClients_;

    volatile bool finish_;

    /**
    * a state of the game.
    *
    */
    GameState gameState_;

    /**
    * maximum number of supported players in the game.
    *
    */
    const unsigned MAX_PLAYERS = 2;

    /**
    * minimum number of players, who are necessary to start the game.
    *
    */
    const unsigned MIN_PLAYERS = 2;

    /**
    * This method sends a \p msg over the \p connection.
    *
    */
    void sendMessage(const Json::Value& msg, PConnection connection);

    /**
    * This method controls a new received \p message from \p connection.
    *
    */
    void onConnectionMessage(PConnection connection, PMessage message);

    /**
    * This method controls opening the \p connection.
    *
    */
    void onConnectionOpen(PConnection connection);

    /**
    * This method controls closing the \p connection by the \p reason and with \p status.
    *
    */
    void onConnectionClose(PConnection connection, int status, const std::string& reason);

    /**
    * This method controls the connection error.
    *
    */
    void onConnectionError(PConnection connection, const WsError& error);

    /**
    * This method controls sending error.
    *
    */
    void onSendingError(const WsError& ec);

    /**
    * This method controls move requests.
    *
    */
    void onBombermanMove(Json::Value value);

    /**
    * This method controls plant requests.
    *
    */
    void onBombermanPlantBomb(Json::Value value);

    /**
    * This method returns the json value of the fields information.
    *
    * \param all_fields if TRUE all fields are consider to create the fields information,
    * if FALSE only changed fields are consider to create the fields information
    */
    Json::Value fieldsInfoToJson(bool all_fields = false);

    /**
    * This method returns the json value of the bomberman position.
    *
    */
    Json::Value bombermansPositonToJson();

    /**
    * This method returns the json value of the bomberman stats, corresponding to the \p bomerman_id.
    *
    */
    Json::Value bombermanStatsToJson(unsigned bomberman_id);

    /**
    * This method returns the json value of the player id , corresponding to the \p bomerman_id.
    *
    */
    Json::Value playerIdToJson(unsigned bomberman_id);

    /**
    * This method returns the json value of the map information.
    *
    */
    Json::Value mapInfoToJson();

    /**
    * This method returns the json value of the game start.
    *
    */
    Json::Value startGameToJson();

    /**
    * This method returns the json value of the game end, corresponding to the game \p result.
    *
    */
    Json::Value endGameToJson(Result result);

    /**
    * This method returns available bomberman id, which gets player.
    *
    */
    unsigned getAvailableBombermanId();

};

#endif /* BM_GAMEMASTER_H */

#!/bin/bash

which lcov 1>/dev/null 2>&1
if [ $? != 0 ]
then
    echo "You need to have lcov installed in order to generate the test coverage report"
    exit 1
fi

if [ ! -e "./CMakeLists.txt" ]; then
   echo "No ./CMakeLists.txt file found. Probably not a cmake workspace."
   exit 1
fi

find ./build/ -name '*.gc*' -print0 | xargs -0 -I {} rm "{}"

rm -rf ./coverage-stats/ ./build/
mkdir coverage-stats
mkdir build

cd build
cmake ..
make
cd ..

mapfile -t OUTPUT < <(find ./build/ -name '*.gcno') #all the folders that contains compiled objects
OUTPUT=("${OUTPUT[@]%/*}")
mapfile -t OUTPUT < <(printf '%s\n' "${OUTPUT[@]}" | sort | uniq)
lcov --base-directory ./ ${OUTPUT[@]/#/--directory } --capture --initial --output-file ./coverage-stats/lcovOutput_base.info

cd build
cmake ..
ctest
cd ..

mapfile -t OUTPUT_T < <(find ./build/ -name '*.gcda') #all the folders that contains executed objects
OUTPUT_T=("${OUTPUT_T[@]%/*}")
mapfile -t OUTPUT_T < <(printf '%s\n' "${OUTPUT_T[@]}" | sort | uniq)
lcov --base-directory ./ ${OUTPUT_T[@]/#/--directory } --ignore-errors gcov,source --capture --output-file ./coverage-stats/lcovOutput_test.info

lcov --add-tracefile ./coverage-stats/lcovOutput_base.info --add-tracefile ./coverage-stats/lcovOutput_test.info  --output-file ./coverage-stats/lcovOutput_total.info
lcov --remove ./coverage-stats/lcovOutput_total.info '/usr/include/*' '/usr/lib/*' '/usr/local/*' -o ./coverage-stats/lcovOutput_filtered.info
genhtml ./coverage-stats/lcovOutput_filtered.info --legend --output-directory=coverage-stats

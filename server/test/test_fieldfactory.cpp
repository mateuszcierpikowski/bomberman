/*
 * Project: Bomberman
 * Author: mc & md
 * File:   test_additionfactory.cpp
 */

 /**
  * @file test_additionfactory.cpp
  *
  * File contains test of factory classes for fields.
  */

#define BOOST_TEST_MODULE TestMap
#include <boost/test/unit_test.hpp>

#include "bomberman.hpp"

#include "factoryfielddestructible.hpp"
#include "factoryfieldindestructible.hpp"

/**
 * Test the functionality of the class FactoryFieldIndestructible.
 */
BOOST_AUTO_TEST_CASE(FieldFactory_Indestructible)
{
    PFactoryField factoryField = PFactoryField(new FactoryFieldIndestructible);

    FieldAbs::FieldData type = FieldAbs::FieldData(MapPosition(0,0), FieldAbs::Type::INDESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::NONE);

    PField field = factoryField->createField(type);

    FieldAbs::FieldData type_check;

    field->getFieldData(type_check);

    BOOST_CHECK(type_check.type_ == FieldAbs::Type::INDESTRUCTIBLE);
    BOOST_CHECK(type_check.subType_ == FieldAbs::SubType::EMPTY);
    BOOST_CHECK(type_check.additionType_ == AdditionAbs::AdditionType::NONE);

    BOOST_CHECK(typeid(*field) == typeid(FieldIndestructible));

}

/**
 * Test the functionality of the class FactoryFieldDestructible.
 */
BOOST_AUTO_TEST_CASE(FieldFactory_Destructible)
{
    PFactoryField factoryField = PFactoryField(new FactoryFieldDestructible);

    PBomberman bomberman = PBomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
    // field without addition
    FieldAbs::FieldData type = FieldAbs::FieldData(MapPosition(0,0), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::NONE);

    PField field = factoryField->createField(type);

    FieldAbs::FieldData type_check;

    field->getFieldData(type_check);

    BOOST_CHECK(type_check.type_ == FieldAbs::Type::DESTRUCTIBLE);
    BOOST_CHECK(type_check.subType_ == FieldAbs::SubType::EMPTY);
    BOOST_CHECK(type_check.additionType_ == AdditionAbs::AdditionType::NONE);

    BOOST_CHECK(typeid(*field) == typeid(FieldDestructible));

    field->addBomberman(bomberman);

    BOOST_CHECK_EQUAL(bomberman->getBombRange(), 1);
    BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 1);
    BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::NORMAL);

    // field with AdditionType::EXTRA_RANGE
    type = FieldAbs::FieldData(MapPosition(0,0), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::EXTRA_RANGE);

    field = factoryField->createField(type);

    field->getFieldData(type_check);

    BOOST_CHECK(type_check.type_ == FieldAbs::Type::DESTRUCTIBLE);
    BOOST_CHECK(type_check.subType_ == FieldAbs::SubType::EMPTY);
    BOOST_CHECK(type_check.additionType_ == AdditionAbs::AdditionType::EXTRA_RANGE);

    BOOST_CHECK(typeid(*field) == typeid(FieldDestructible));

    field->addBomberman(bomberman);

    BOOST_CHECK_EQUAL(bomberman->getBombRange(), 2);
    BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 1);
    BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::NORMAL);

    // field with AdditionType::EXTRA_BOMB
    type = FieldAbs::FieldData(MapPosition(0,0), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::EXTRA_BOMB);

    field = factoryField->createField(type);

    field->getFieldData(type_check);

    BOOST_CHECK(type_check.type_ == FieldAbs::Type::DESTRUCTIBLE);
    BOOST_CHECK(type_check.subType_ == FieldAbs::SubType::EMPTY);
    BOOST_CHECK(type_check.additionType_ == AdditionAbs::AdditionType::EXTRA_BOMB);

    BOOST_CHECK(typeid(*field) == typeid(FieldDestructible));

    field->addBomberman(bomberman);

    BOOST_CHECK_EQUAL(bomberman->getBombRange(), 2);
    BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 2);
    BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::NORMAL);

    // field with AdditionType::INVERT_MODE
    type = FieldAbs::FieldData(MapPosition(0,0), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::INVERT_MODE);

    field = factoryField->createField(type);

    field->getFieldData(type_check);

    BOOST_CHECK(type_check.type_ == FieldAbs::Type::DESTRUCTIBLE);
    BOOST_CHECK(type_check.subType_ == FieldAbs::SubType::EMPTY);
    BOOST_CHECK(type_check.additionType_ == AdditionAbs::AdditionType::INVERT_MODE);

    BOOST_CHECK(typeid(*field) == typeid(FieldDestructible));

    field->addBomberman(bomberman);

    BOOST_CHECK_EQUAL(bomberman->getBombRange(), 2);
    BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 2);
    BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::INVERT);

}

/*
 * Project: Bomberman
 * Author: mc & md
 * File:   test_addition.cpp
 */

 /**
  * @file test_addition.cpp
  *
  * File contains test of classes that are associated with additions.
  */

#define BOOST_TEST_MODULE TetsAddition
#include <boost/test/unit_test.hpp>

#include "additionabs.hpp"
#include "additionnegativeinvertmode.hpp"
#include "additionpositiveextrabomb.hpp"
#include "additionpositiveextrarange.hpp"
#include "bomberman.hpp"

/**
 * Test the functionality of the class AdditionNegativeInvertMode.
 */
BOOST_AUTO_TEST_CASE(TestAdditionNegativeInvertMode)
{
  PAddition addition(new AdditionNegativeInvertMode(2));

  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));

  BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::NORMAL);

  addition->execute(bomberman);

  BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::INVERT);

  BOOST_CHECK(addition->getType() == AdditionAbs::AdditionType::INVERT_MODE);
}

/**
 * Test the functionality of the class AdditionPositiveExtraBomb.
 */
BOOST_AUTO_TEST_CASE(TestAdditionPositiveExtraBomb)
{
  PAddition addition(new AdditionPositiveExtraBomb());

  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));

  BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 1);

  addition->execute(bomberman);

  BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 2);

  BOOST_CHECK(addition->getType() == AdditionAbs::AdditionType::EXTRA_BOMB);
}

/**
 * Test the functionality of the class AdditionPositiveExtraRange.
 */
BOOST_AUTO_TEST_CASE(TestAdditionPositiveExtraRange)
{
  PAddition addition(new AdditionPositiveExtraRange());

  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));

  BOOST_CHECK_EQUAL(bomberman->getBombRange(), 1);

  addition->execute(bomberman);

  BOOST_CHECK_EQUAL(bomberman->getBombRange(), 2);

  BOOST_CHECK(addition->getType() == AdditionAbs::AdditionType::EXTRA_RANGE);
}

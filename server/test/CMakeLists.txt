##
# Project: Bomberman
# Author: mc & md
# File:   CMakeLists.txt - test
##

## Flags ##
add_definitions (-DBOOST_TEST_DYN_LINK)

## Tests ##
add_executable (test_addition
  test_addition.cpp
)

target_link_libraries (test_addition
  bomberman_server
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)

add_executable (test_bomb
  test_bomb.cpp
)

target_link_libraries (test_bomb
  bomberman_server
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)

add_executable (test_bomberman
  test_bomberman.cpp
)

target_link_libraries (test_bomberman
  bomberman_server
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)

add_executable (test_field
  test_field.cpp
)

target_link_libraries (test_field
  bomberman_server
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)

add_executable (test_map
  test_map.cpp
)

target_link_libraries (test_map
  bomberman_server
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)

add_executable (test_additionfactory
  test_additionfactory.cpp
)

target_link_libraries (test_additionfactory
  bomberman_server
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)

add_executable (test_fieldfactory
  test_fieldfactory.cpp
)

target_link_libraries (test_fieldfactory
  bomberman_server
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
)

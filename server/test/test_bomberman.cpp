/*
 * Project: Bomberman
 * Author: mc & md
 * File:   test_bomberman.cpp
 */

 /**
  * @file test_bomberman.cpp
  *
  * File contains test of Bomberman class.
  */

#define BOOST_TEST_MODULE TestBomberman
#include <boost/test/unit_test.hpp>
#include <boost/thread/thread.hpp>

#include "bomberman.hpp"

/**
 * Test position setting for class Bomberman.
 */
BOOST_AUTO_TEST_CASE(TestBombermanPosition)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(1, 2), FieldPosition(0, 4))));

  Position position = bomberman->getPosition();

  BOOST_CHECK_EQUAL(position.mapPosition_.mapX_, 1);
  BOOST_CHECK_EQUAL(position.mapPosition_.mapY_, 2);
  BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);
  BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 4);

  bomberman->setPosition(Position(MapPosition(2, 1), FieldPosition(-4, 0)));

  position = bomberman->getPosition();

  BOOST_CHECK_EQUAL(position.mapPosition_.mapX_, 2);
  BOOST_CHECK_EQUAL(position.mapPosition_.mapY_, 1);
  BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, -4);
  BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
}

/**
 * Test mode setting for class Bomberman.
 */
BOOST_AUTO_TEST_CASE(TestBombermanMode)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));

  BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::NORMAL);

  bomberman->setInvertMode(0.001);

  BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::INVERT);

  boost::this_thread::sleep(boost::posix_time::milliseconds(1));

  BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::NORMAL);
}

/**
 * Test lives setting for class Bomberman.
 */
BOOST_AUTO_TEST_CASE(TestBombermanLives)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));

  BOOST_CHECK_EQUAL(bomberman->getLives(), 1);

  bomberman->increaseLives();

  BOOST_CHECK_EQUAL(bomberman->getLives(), 2);

  bomberman->decreaseLives();

  BOOST_CHECK_EQUAL(bomberman->getLives(), 1);
  BOOST_CHECK_EQUAL(bomberman->anyLiveRemain(), true);

  bomberman->decreaseLives();

  BOOST_CHECK_EQUAL(bomberman->anyLiveRemain(), false);
}

/**
 * Test bomb number setting for class Bomberman.
 */
BOOST_AUTO_TEST_CASE(TestBomberman_BombNumber)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));

  BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 1);

  bomberman->increaseBombsNumber();

  BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 2);

  bomberman->decreaseBombsNumber();

  BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 1);
  BOOST_CHECK_EQUAL(bomberman->anyBombRemain(), true);

  bomberman->decreaseBombsNumber();

  BOOST_CHECK_EQUAL(bomberman->anyBombRemain(), false);
}

/**
 * Test bomb range setting for class Bomberman.
 */
BOOST_AUTO_TEST_CASE(TestBomberman_BombRange)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));

  BOOST_CHECK_EQUAL(bomberman->getBombRange(), 1);

  bomberman->increaseBombRange();
  BOOST_CHECK_EQUAL(bomberman->getBombRange(), 2);
}

/**
 * Test function anyAttributeChange in every case for class Bomberman.
 */
BOOST_AUTO_TEST_CASE(TestBomberman_anyAttributeChange)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));

  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), true);

  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), false);
  bomberman->increaseLives();
  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), true);

  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), false);
  bomberman->decreaseLives();
  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), true);

  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), false);
  bomberman->increaseBombsNumber();
  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), true);

  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), false);
  bomberman->decreaseBombsNumber();
  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), true);

  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), false);
  bomberman->increaseBombRange();
  BOOST_CHECK_EQUAL(bomberman->anyAttributeChange(), true);

}

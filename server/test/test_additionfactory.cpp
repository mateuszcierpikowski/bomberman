/*
 * Project: Bomberman
 * Author: mc & md
 * File:   test_additionfactory.cpp
 */

 /**
  * @file test_additionfactory.cpp
  *
  * File contains test of factory classes for additions.
  */
#define BOOST_TEST_MODULE TestMap
#include <boost/test/unit_test.hpp>

#include "factoryaddition.hpp"
#include "additionpositiveextrabomb.hpp"
#include "additionpositiveextrarange.hpp"
#include "additionnegativeinvertmode.hpp"


AdditionAbs* CreateAdditionExtraBomb() {
    return new AdditionPositiveExtraBomb();
}

AdditionAbs* CreateAdditionExtraRange() {
    return new AdditionPositiveExtraRange();
}

AdditionAbs* CreateAdditionInvertMode() {
    return new AdditionNegativeInvertMode(1);
}

/**
 * Test the functionality of the class AdditonFactory.
 */
BOOST_AUTO_TEST_CASE(AdditonFactory)
{
    FactoryAddition& factory = FactoryAddition::getInstance();

    factory.registerAddition(AdditionAbs::AdditionType::EXTRA_BOMB, CreateAdditionExtraBomb);
    factory.registerAddition(AdditionAbs::AdditionType::EXTRA_RANGE, CreateAdditionExtraRange);
    factory.registerAddition(AdditionAbs::AdditionType::INVERT_MODE, CreateAdditionInvertMode);

    PAddition addition =  factory.create(AdditionAbs::AdditionType::EXTRA_BOMB);

    BOOST_CHECK(addition->getType() == AdditionAbs::AdditionType::EXTRA_BOMB);

    addition =  factory.create(AdditionAbs::AdditionType::EXTRA_RANGE);

    BOOST_CHECK(addition->getType() == AdditionAbs::AdditionType::EXTRA_RANGE);

    addition =  factory.create(AdditionAbs::AdditionType::INVERT_MODE);

    BOOST_CHECK(addition->getType() == AdditionAbs::AdditionType::INVERT_MODE);

}

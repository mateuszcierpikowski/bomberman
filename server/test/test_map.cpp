/*
 * Project: Bomberman
 * Author: mc & md
 * File:   test_map.cpp
 */

 /**
  * @file test_map.cpp
  *
  * File contains test of classes that are associated with map.
  */

#define BOOST_TEST_MODULE TestMap
#include <boost/test/unit_test.hpp>
#include <boost/thread/thread.hpp>

#include "maptest.hpp"
#include "mapnormal.hpp"

/**
 * Test constructor ofclass MapNormal.
 */
BOOST_AUTO_TEST_CASE(TestMap_MapNormal)
{
    PMap map_normal = PMap(new MapNormal(19, 11, 10, 16, 4, 70, 1, 1));

    VVFields vv_fields = map_normal->getFieldList();

    FieldAbs::FieldData field_data;

    vv_fields.at(0).at(4)->getFieldData(field_data);
    BOOST_CHECK(field_data.type_ == FieldAbs::Type::INDESTRUCTIBLE);

    vv_fields.at(6).at(4)->getFieldData(field_data);
    BOOST_CHECK(field_data.type_ == FieldAbs::Type::INDESTRUCTIBLE);

    vv_fields.at(4).at(16)->getFieldData(field_data);
    BOOST_CHECK(field_data.type_ == FieldAbs::Type::INDESTRUCTIBLE);

    vv_fields.at(4).at(5)->getFieldData(field_data);
    BOOST_CHECK(field_data.type_ == FieldAbs::Type::DESTRUCTIBLE);

    vv_fields.at(5).at(16)->getFieldData(field_data);
    BOOST_CHECK(field_data.type_ == FieldAbs::Type::DESTRUCTIBLE);

    vv_fields.at(9).at(17)->getFieldData(field_data);
    BOOST_CHECK(field_data.type_ == FieldAbs::Type::DESTRUCTIBLE);
}

/**
 * Test function bombermanMove for class MapAbs.
 */
BOOST_AUTO_TEST_CASE(TestMap_bombermanMove)
{
    int ID = 0;

    Move move;
    Position position;

    PMap map_test = PMap(new MapTest());
    VBombermans vBombermans = map_test->getBombermanList();
    PBomberman bomberman = vBombermans.at(ID);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    // position on field (0,0)
    bomberman->setPosition(Position(MapPosition(1,1), FieldPosition(0,0)));
    move = Move(1, 0, 0, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(1,1), FieldPosition(0,0)));
    move = Move(0, 0, 1, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(1,1), FieldPosition(0,0)));
    move = Move(0, 1, 0, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 1);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(1,1), FieldPosition(0,0)));
    move = Move(0, 0, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 1);

    bomberman->setPosition(Position(MapPosition(1,1), FieldPosition(0,0)));
    move = Move(0, 1, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    // position on field (-1/1,0)
    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(1,0)));
    move = Move(0, 0, 1, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(1,0)));
    move = Move(1, 0, 1, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, -1);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(1,0)));
    move = Move(0, 1, 0, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 1);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(1,0)));
    move = Move(0, 1, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 2);

    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(1,0)));
    move = Move(0, 0, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 2);

    // position on field (x,0)
    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(5,0)));
    move = Move(0, 1, 0, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 4);

    bomberman->setPosition(Position(MapPosition(3,5), FieldPosition(-5,0)));
    move = Move(1, 0, 0, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, -4);

    // position on field (0,-1/1)
    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(0,-1)));
    move = Move(0, 1, 0, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(0,-1)));
    move = Move(0, 1, 1, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(0,-1)));
    move = Move(0, 1, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 1);

    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(0,-1)));
    move = Move(1, 0, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, -2);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(0,-1)));
    move = Move(1, 0, 0, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, -2);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    // position on field (0,x)
    bomberman->setPosition(Position(MapPosition(3,5), FieldPosition(0,-5)));
    move = Move(0, 0, 1, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, -4);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    bomberman->setPosition(Position(MapPosition(1,1), FieldPosition(0,5)));
    move = Move(0, 0, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 4);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);

    // position on field (max/min, 0)/(0,max/min)
    bomberman->setPosition(Position(MapPosition(5,3), FieldPosition(0,-10)));
    move = Move(1, 0, 0, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 9);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapY_, 2);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapX_, 5);

    bomberman->setPosition(Position(MapPosition(1,2), FieldPosition(0,10)));
    move = Move(0, 1, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, -10);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapY_, 3);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapX_, 1);

    bomberman->setPosition(Position(MapPosition(7,3), FieldPosition(-10,0)));
    move = Move(0, 1, 1, 0);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 9);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapY_, 3);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapX_, 6);

    bomberman->setPosition(Position(MapPosition(3,5), FieldPosition(10,0)));
    move = Move(1, 0, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 0);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, -10);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapY_, 5);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapX_, 4);

    // for bomberman in INVERT_MODE
    bomberman->setInvertMode(1.0);
    bomberman->setPosition(Position(MapPosition(1,2), FieldPosition(0,7)));
    move = Move(1, 0, 0, 1);
    map_test->bombermanMove(ID, move);

    position = bomberman->getPosition();
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldY_, 8);
    BOOST_CHECK_EQUAL(position.fieldPosition_.fieldX_, 0);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapY_, 2);
    BOOST_CHECK_EQUAL(position.mapPosition_.mapX_, 1);
}

/**
 * Test function bombermanPlantBomb for class MapAbs.
 */
BOOST_AUTO_TEST_CASE(TestMap_bombermanPlantBomb)
{
    int ID = 0;

    PMap map_test = PMap(new MapTest());
    VBombermans vBombermans = map_test->getBombermanList();
    PBomberman bomberman = vBombermans.at(ID);

    VVFields fields = map_test->getFieldList();

    map_test->bombermanPlantBomb(ID);

    BOOST_CHECK_EQUAL(map_test->getBombList().size(), 1);

    map_test->bombermanPlantBomb(ID);

    BOOST_CHECK_EQUAL(map_test->getBombList().size(), 1);

    bomberman->increaseBombsNumber();

    map_test->bombermanPlantBomb(ID);

    BOOST_CHECK_EQUAL(map_test->getBombList().size(), 1);

    VResult result = map_test->checkBombermans();

    BOOST_CHECK(result[ID] == Result::GAME_RUN);

    boost::this_thread::sleep(boost::posix_time::milliseconds(1));

    map_test->checkBombs();

    FieldAbs::FieldData field_data;

    fields.at(1).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(2).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(1).at(2)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(1).at(3)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::BARRICADE);

    fields.at(3).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    result = map_test->checkBombermans();

    BOOST_CHECK(result[ID] == Result::DEFEAT);

    boost::this_thread::sleep(boost::posix_time::milliseconds(1));

    map_test->checkBombs();

    fields.at(1).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(2).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(1).at(2)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(1).at(3)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::BARRICADE);

    fields.at(3).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);
}

/**
 * Test explosion propagation for class MapAbs.
 */
BOOST_AUTO_TEST_CASE(TestMap_propagete_explosion) {

    PMap map_test = PMap(new MapTest());
    VBombermans vBombermans = map_test->getBombermanList();
    PBomberman bomberman_1 = vBombermans.at(0);
    PBomberman bomberman_2 = vBombermans.at(1);

    VVFields fields = map_test->getFieldList();

    bomberman_1->increaseBombRange();

    map_test->bombermanPlantBomb(0);

    BOOST_CHECK_EQUAL(map_test->getBombList().size(), 1);

    map_test->bombermanPlantBomb(1);

    BOOST_CHECK_EQUAL(map_test->getBombList().size(), 2);

    boost::this_thread::sleep(boost::posix_time::milliseconds(1));

    map_test->checkBombs();

    FieldAbs::FieldData field_data;

    fields.at(1).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(1).at(2)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(1).at(3)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(2).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(3).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(3).at(2)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);

    fields.at(3).at(3)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::BARRICADE);

    boost::this_thread::sleep(boost::posix_time::milliseconds(1));

    map_test->checkBombs();

    fields.at(1).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(1).at(2)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(1).at(3)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(2).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(3).at(1)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(3).at(2)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);

    fields.at(3).at(3)->getFieldData(field_data);

    BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::BARRICADE);

    VResult result = map_test->checkBombermans();

    BOOST_CHECK(result[0] == Result::DRAW);

    BOOST_CHECK(result[1] == Result::DRAW);

}

/**
 * Test game result for class MapAbs.
 */
BOOST_AUTO_TEST_CASE(TestMap_game_result) {

    PMap map_test = PMap(new MapTest(true));
    VBombermans vBombermans = map_test->getBombermanList();
    PBomberman bomberman = vBombermans.at(0);

    map_test->bombermanPlantBomb(0);

    boost::this_thread::sleep(boost::posix_time::milliseconds(1));

    map_test->checkBombs();

    Move move = Move(1, 0, 0, 0);

    for(int i = 0; i < 11; ++i)
        map_test->bombermanMove(1, move);

    boost::this_thread::sleep(boost::posix_time::milliseconds(1));

    map_test->checkBombs();

    VResult result = map_test->checkBombermans();

    BOOST_CHECK(result[0] == Result::DEFEAT);

    BOOST_CHECK(result[1] == Result::DEFEAT);

    BOOST_CHECK(result[2] == Result::GAME_RUN);

    BOOST_CHECK(result[3] == Result::GAME_RUN);

}

/*
 * Project: Bomberman
 * Author: mc & md
 * File:   test_bomb.cpp
 */

 /**
  * @file test_bomb.cpp
  *
  * File contains test of Bomb class.
  */

#define BOOST_TEST_MODULE TestBomb
#include <boost/test/unit_test.hpp>
#include <boost/thread/thread.hpp>

#include "bomberman.hpp"
#include "bomb.hpp"

/**
 * Test transitions between states for class Bomb.
 */
BOOST_AUTO_TEST_CASE(TestBomb_State)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PBomb bomb(new Bomb(MapPosition(1, 2), 0.001, 0.001, bomberman));

  BOOST_CHECK(bomb->getState() == Bomb::State::COUNT_DOWN);

  boost::this_thread::sleep(boost::posix_time::milliseconds(1));

  BOOST_CHECK(bomb->getState() == Bomb::State::START_EXPLOSION);

  BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 0);

  bomb->dentonate();

  BOOST_CHECK(bomb->getState() == Bomb::State::EXPLOSION);

  boost::this_thread::sleep(boost::posix_time::milliseconds(1));

  BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 1);

  BOOST_CHECK(bomb->getState() == Bomb::State::STOP_EXPLOSION);
}

/**
 * Test position setting for class Bomb.
 */
BOOST_AUTO_TEST_CASE(TestBomb_Position)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PBomb bomb(new Bomb(MapPosition(1, 2), 2, 2, bomberman));

  MapPosition map_position = bomb->getPosition();

  BOOST_CHECK_EQUAL(map_position.mapX_, 1);
  BOOST_CHECK_EQUAL(map_position.mapY_, 2);
}

/**
 * Test range setting for class Bomb.
 */
BOOST_AUTO_TEST_CASE(TestBomb_Range)
{
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PBomb bomb(new Bomb(MapPosition(1, 2), 2, 2, bomberman));

  BOOST_CHECK_EQUAL(bomb->getRange(), 1);

  bomberman->increaseBombRange();

  BOOST_CHECK_EQUAL(bomb->getRange(), 2);
}

/*
 * Project: Bomberman
 * Author: mc & md
 * File:   test_field.cpp
 */

 /**
  * @file test_field.cpp
  *
  * File contains test of classes that are associated with field.
  */

#define BOOST_TEST_MODULE TetsField
#include <boost/test/unit_test.hpp>
#include <boost/thread/thread.hpp>

#include "fieldabs.hpp"
#include "fielddestructible.hpp"
#include "fieldindestructible.hpp"
#include "bomberman.hpp"
#include "bomb.hpp"
#include "additionabs.hpp"
#include "additionnegativeinvertmode.hpp"
#include "additionpositiveextrabomb.hpp"
#include "additionpositiveextrarange.hpp"

/**
 * Test function checkMove for class FieldDestructible.
 */
BOOST_AUTO_TEST_CASE(TestFieldDestructible_checkMove)
{
  FieldAbs::FieldData field_data(MapPosition(1,1), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::BARRICADE, AdditionAbs::AdditionType::NONE);
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PBomb bomb(new Bomb(MapPosition(1, 2), 2, 2, bomberman));
  PBomb bomb_chain_reaction;

  PField field(new FieldDestructible(field_data, nullptr));

  BOOST_CHECK_EQUAL(field->stopBombExplosion(bomb), false);
  // FieldAbs::SubType::BARRICADE
  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.type_ == FieldAbs::Type::DESTRUCTIBLE);
  BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::BARRICADE);
  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);

  BOOST_CHECK_EQUAL(field->checkMove(), false);

  // FieldAbs::SubType::EXPLOSION
  BOOST_CHECK_EQUAL(field->startBombExplosion(bomb, bomb_chain_reaction), false);

  BOOST_CHECK_EQUAL(field->checkMove(), true);
  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.type_ == FieldAbs::Type::DESTRUCTIBLE);
  BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);
  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);

  // FieldAbs::SubType::EMPTY
  BOOST_CHECK_EQUAL(field->stopBombExplosion(bomb), true);

  BOOST_CHECK_EQUAL(field->checkMove(), true);
  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.type_ == FieldAbs::Type::DESTRUCTIBLE);
  BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);
  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);

  BOOST_CHECK_EQUAL(field->checkMove(), true);

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), false);

  bomb = PBomb(new Bomb(MapPosition(1, 2), 2, 2, bomberman));
  // FieldAbs::SubType::PLANTED
  field->addBomb(bomb);

  BOOST_CHECK_EQUAL(field->isBombPlanted(), true);
  BOOST_CHECK_EQUAL(field->checkMove(), false);
  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.type_ == FieldAbs::Type::DESTRUCTIBLE);
  BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::PLANTED);
  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);

  BOOST_CHECK_EQUAL(field->startBombExplosion(bomb, bomb_chain_reaction), true);

  BOOST_CHECK_EQUAL(field->isBombPlanted(), false);
  BOOST_CHECK_EQUAL(field->checkMove(), true);
  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.type_ == FieldAbs::Type::DESTRUCTIBLE);
  BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EXPLOSION);
  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);
}

/**
 * Test the functionality of the bomberman assigned to the field for class FieldDestructible.
 */
BOOST_AUTO_TEST_CASE(TestFieldDestructible_Bomberman)
{
  FieldAbs::FieldData field_data(MapPosition(1,1), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::NONE);
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PBomb bomb(new Bomb(MapPosition(1, 2), 2, 2, bomberman));
  PBomb bomb_chain_reaction;

  PField field(new FieldDestructible(field_data, nullptr));

  bomberman->increaseLives();

  BOOST_CHECK_EQUAL(bomberman->getLives(), 2);

  field->addBomberman(bomberman);
  field->startBombExplosion(bomb, bomb_chain_reaction);

  BOOST_CHECK_EQUAL(bomberman->getLives(), 1);

  field->stopBombExplosion(bomb);
  field->deleteBomberman(bomberman);
  field->startBombExplosion(bomb, bomb_chain_reaction);

  BOOST_CHECK_EQUAL(bomberman->getLives(), 1);
}

/**
 * Test of the AdditionNegativeInvertMode on the field for class FieldDestructible.
 */
BOOST_AUTO_TEST_CASE(TestFieldDestructible_AdditionNegativeInvertMode)
{
  FieldAbs::FieldData field_data(MapPosition(1,1), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::INVERT_MODE);
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PAddition addition(new AdditionNegativeInvertMode(0.001));
  PField field(new FieldDestructible(field_data, addition));

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::INVERT_MODE);

  field->addBomberman(bomberman);

  BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::INVERT);

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);

  boost::this_thread::sleep(boost::posix_time::milliseconds(1));

  BOOST_CHECK(bomberman->getMode() == Bomberman::Mode::NORMAL);
}

/**
 * Test of the AdditionPositiveExtraBomb on the field for class FieldDestructible.
 */
BOOST_AUTO_TEST_CASE(TestFieldDestructible_AdditionPositiveExtraBomb)
{
  FieldAbs::FieldData field_data(MapPosition(1,1), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::EXTRA_BOMB);
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PAddition addition(new AdditionPositiveExtraBomb());
  PField field(new FieldDestructible(field_data, addition));

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::EXTRA_BOMB);

  field->addBomberman(bomberman);

  BOOST_CHECK_EQUAL(bomberman->getBombsNumber(), 2);

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);
}

/**
 * Test of the AdditionPositiveExtraRange on the field for class FieldDestructible.
 */
BOOST_AUTO_TEST_CASE(TestFieldDestructible_AdditionPositiveExtraRange)
{
  FieldAbs::FieldData field_data(MapPosition(1,1), FieldAbs::Type::DESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::EXTRA_RANGE);
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PAddition addition(new AdditionPositiveExtraRange());
  PField field(new FieldDestructible(field_data, addition));

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::EXTRA_RANGE);

  field->addBomberman(bomberman);

  BOOST_CHECK_EQUAL(bomberman->getBombRange(), 2);

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);
}
/**
 * Test the functionality of the class FieldIndestructible.
 */
BOOST_AUTO_TEST_CASE(TestFieldIndestructible)
{
  FieldAbs::FieldData field_data(MapPosition(1,1), FieldAbs::Type::INDESTRUCTIBLE, FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::NONE);
  PField field(new FieldIndestructible(field_data));
  PBomberman bomberman(new Bomberman(Position(MapPosition(0, 0), FieldPosition(0, 0))));
  PBomb bomb(new Bomb(MapPosition(1, 2), 2, 2, bomberman));
  PBomb bomb_chain_reaction;

  field->addBomberman(bomberman);
  field->addBomb(bomb);
  field->deleteBomberman(bomberman);

  BOOST_CHECK_EQUAL(field->checkMove(), false);

  BOOST_CHECK_EQUAL(field->startBombExplosion(bomb, bomb_chain_reaction), false);

  BOOST_CHECK_EQUAL(field->stopBombExplosion(bomb), false);

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), true);

  BOOST_CHECK(field_data.type_ == FieldAbs::Type::INDESTRUCTIBLE);
  BOOST_CHECK(field_data.subType_ == FieldAbs::SubType::EMPTY);
  BOOST_CHECK(field_data.additionType_ == AdditionAbs::AdditionType::NONE);

  BOOST_CHECK_EQUAL(field->getFieldData(field_data), false);
}

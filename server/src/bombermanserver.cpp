/*
 * Project: Bomberman
 * Author: mc & md
 * File:   bombermanserver.cpp
 */

#include <boost/thread/thread.hpp>

#include "gamemaster.hpp"

int main(int argc, char** argv) {

    GameMaster game;

    boost::thread_group thread_group;

    thread_group.create_thread(boost::bind(&GameMaster::start, &game));

    thread_group.create_thread(boost::bind(&GameMaster::trigger, &game));

    thread_group.join_all();

    return 0;
}

/*
 * Project: Bomberman
 * Author: mc & md
 * File:   additionpositiveextrabomb.cpp
 */

#include "additionpositiveextrabomb.hpp"

AdditionPositiveExtraBomb::AdditionPositiveExtraBomb() { }

AdditionPositiveExtraBomb::~AdditionPositiveExtraBomb() { }

void AdditionPositiveExtraBomb::execute (const PBomberman& bomberman) {
    bomberman->increaseBombsNumber();
}

AdditionAbs::AdditionType AdditionPositiveExtraBomb::getType() {
  return AdditionType::EXTRA_BOMB;
}

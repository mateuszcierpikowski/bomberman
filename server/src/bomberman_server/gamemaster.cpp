/*
 * Project: Bomberman
 * Author: mc & md
 * File:   gamemaster.cpp
 */

#include "gamemaster.hpp"

GameMaster::GameMaster() : finish_(false), bomermansMove_(2), bomermansPlant_(2, false), gameState_(GameState::WAIT_FOR_ALL_PLAYERS)
{
    server_ = PWebSocket(new WebSocket(8383, 4));
    map_ = PMap(new MapNormal(19, 11, 6, 16, 4, 70, 4, 2));

    auto& ws_endpoint = server_->endpoint["^/bombermanGame/?$"];

    ws_endpoint.onmessage = std::bind(&GameMaster::onConnectionMessage, this, std::placeholders::_1, std::placeholders::_2);
    ws_endpoint.onopen = std::bind(&GameMaster::onConnectionOpen, this, std::placeholders::_1);
    ws_endpoint.onclose = std::bind(&GameMaster::onConnectionClose, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    ws_endpoint.onerror = std::bind(&GameMaster::onConnectionError, this, std::placeholders::_1, std::placeholders::_2);

}

GameMaster::~GameMaster() {

}

void GameMaster::start() {
    server_->start();
}

void GameMaster::stop() {
    finish_ = true;
    server_->stop();
}

void GameMaster::trigger() {

    while (!finish_) {

        if (gameState_ == GameState::WAIT_FOR_ALL_PLAYERS) {
            boost::this_thread::sleep(boost::posix_time::milliseconds(40));
            continue;
        }
        mutexClients_.lock();

        map_->checkBombs();

        VResult result_game = map_->checkBombermans();

        for (unsigned i = 0; i < bomermansPlant_.size(); ++i) {

            if (bomermansPlant_.at(i))
                map_->bombermanPlantBomb(i);

            bomermansPlant_.at(i) = false;
        }

        for (unsigned i = 0; i < bomermansMove_.size(); ++i) {
            map_->bombermanMove(i, bomermansMove_.at(i));
        }

        mutexClients_.unlock();

        mutexConnections_.lock();

        Json::Value fieldsInfo = fieldsInfoToJson();

        for (auto connection : connections_) {

            sendMessage(bombermansPositonToJson(), connection.second);

            sendMessage(fieldsInfo, connection.second);

            if (connectionsId_.at(connection.first) < MAX_PLAYERS) {

                if (result_game[connectionsId_[connection.first]] != Result::GAME_RUN)
                    sendMessage(endGameToJson(result_game[connectionsId_[connection.first]]), connection.second);

                if (gameState_ != GameState::END)
                    sendMessage(bombermanStatsToJson(connectionsId_[connection.first]), connection.second);

            }

			if (gameState_ == GameState::END)
				sendMessage(endGameToJson(Result::VICTORY), connection.second);
        }

        mutexConnections_.unlock();

        if (gameState_ == GameState::END)
            gameState_ = GameState::WAIT_FOR_ALL_PLAYERS;

        boost::this_thread::sleep(boost::posix_time::milliseconds(40));
    }

}

void GameMaster::onConnectionMessage(PConnection connection, PMessage message) {

    Json::Value received;
    Json::Reader reader;
    reader.parse(message->string(), received);

    if (received["type"] == "bombermanMove") {
        onBombermanMove(received);
    } else if (received["type"] == "bombermanPlantBomb") {
        onBombermanPlantBomb(received);
    }
}

void GameMaster::onConnectionOpen(PConnection connection) {

    std::cout << "Server: Opened connection " << (size_t)connection.get() << std::endl;

    mutexConnections_.lock();

    unsigned player_id = getAvailableBombermanId();

    connections_[(size_t)connection.get()] = connection;
    connectionsId_[(size_t)connection.get()] =  player_id;


    if (gameState_ == GameState::WAIT_FOR_ALL_PLAYERS && connectionsId_.size() == MAX_PLAYERS) {
        for (auto connection : connections_) {
            sendMessage(startGameToJson(), connection.second);
        }
        gameState_ = GameState::RUN;
    }

    mutexConnections_.unlock();

    sendMessage(mapInfoToJson(), connection);
    sendMessage(fieldsInfoToJson(true), connection);
    sendMessage(playerIdToJson(player_id), connection);

}

void GameMaster::onConnectionError(PConnection connection, const WsError &error) {

}

void GameMaster::onConnectionClose(PConnection connection, int status, const std::string &reason) {

    std::cout << "Server: Closed connection " << (size_t)connection.get() << std::endl;

    mutexConnections_.lock();

    connections_.erase((size_t)connection.get());
    connectionsId_.erase((size_t)connection.get());

    mutexConnections_.unlock();

    if (connectionsId_.size() < MIN_PLAYERS)
		if (gameState_ == GameState::RUN) {
			map_ = PMap(new MapNormal(19, 11, 6, 16, 4, 70, 4, 2));
			gameState_ = GameState::END;
		}

}

void GameMaster::onSendingError(const WsError &ec) {

}

Json::Value GameMaster::fieldsInfoToJson(bool all_fields) {
    Json::Value value;
    Json::Value fields;
    value["type"] = "fieldsInfo";
    for (auto row: map_->getFieldList()) {
        for (auto field: row) {
            FieldAbs::FieldData field_data;
            if (field->getFieldData(field_data) || all_fields) {
                Json::Value tempField;
                tempField["mapX"] = field_data.mapPosition_.mapX_;
                tempField["mapY"] = field_data.mapPosition_.mapY_;
                tempField["type"] = static_cast<int>(field_data.type_);
                tempField["subtype"] = static_cast<int>(field_data.subType_);
                tempField["addition"] = static_cast<int>(field_data.additionType_);
                fields.append(tempField);
            }
        }
    }
    value["data"] = fields;
    return value;

}
Json::Value GameMaster::bombermansPositonToJson() {
    Json::Value value;
    Json::Value bombermansPosition;
    value["type"] = "bombermansPosition";

    for (auto bomberman: map_->getBombermanList()) {
        if (bomberman->getLives()) {
            Json::Value temp;
            temp["mapX"] = bomberman->getPosition().mapPosition_.mapX_;
            temp["mapY"] = bomberman->getPosition().mapPosition_.mapY_;
            temp["fieldX"] = bomberman->getPosition().fieldPosition_.fieldX_;
            temp["fieldY"] = bomberman->getPosition().fieldPosition_.fieldY_;
            bombermansPosition.append(temp);
        }
    }

    value["data"] = bombermansPosition;

    return value;

}

Json::Value GameMaster::bombermanStatsToJson(unsigned bomberman_id) {
    Json::Value value;
    Json::Value bombermanStats;
    value["type"] = "bombermanStats";

    if (!map_->getBombermanList().at(bomberman_id)->anyAttributeChange()) {
        value["data"] = bombermanStats;
        return value;
    }

    bombermanStats["lives"] = map_->getBombermanList().at(bomberman_id)->getLives();
    bombermanStats["bombs"] = map_->getBombermanList().at(bomberman_id)->getBombsNumber();
    bombermanStats["range"] = map_->getBombermanList().at(bomberman_id)->getBombRange();

    value["data"] = bombermanStats;

    return value;

}
Json::Value GameMaster::playerIdToJson(unsigned bomberman_id) {
    Json::Value value;
    value["type"] = "playerId";
    value["data"] = bomberman_id;
    return value;
}

Json::Value GameMaster::mapInfoToJson() {
    Json::Value value;
    Json::Value temp;

    value["type"] = "mapInfo";
    temp["width"] = static_cast<int>(map_->getFieldList().at(0).size());
    temp["height"] = static_cast<int>(map_->getFieldList().size());
    value["data"] = temp;
    return value;
}

Json::Value GameMaster::startGameToJson() {
    Json::Value value;
    value["type"] = "startGame";
    return value;
}

Json::Value GameMaster::endGameToJson(Result result) {
    Json::Value value;
    value["type"] = "endGame";
    value["data"] = static_cast<int>(result);
    return value;
}

unsigned GameMaster::getAvailableBombermanId() {

    if (connectionsId_.size() == 0)
        return 0;

    std::map<unsigned, unsigned> id;

    for (unsigned i = 0; i < MAX_PLAYERS; ++i) {
        id[i] = i;
    }

    for (auto connection : connectionsId_) {
        id.erase(connection.second);
    }

    if (!id.size())
        return MAX_PLAYERS;

    return id.begin()->second;

}

void GameMaster::sendMessage(const Json::Value& msg, PConnection connection) {

    auto send_stream = std::make_shared<WsSendStream>();

    *send_stream << msg << std::endl;

    if (msg.isMember("data") && msg["data"].isNull())
        return;

    server_->send(connection, send_stream,
                  std::bind(&GameMaster::onSendingError, this, std::placeholders::_1));
}


void GameMaster::onBombermanMove(Json::Value value) {

    unsigned id = value["id"].asUInt();

    mutexClients_.lock();

    bomermansMove_.at(id).up_ = value["move"]["up"].asUInt();
    bomermansMove_.at(id).down_ = value["move"]["down"].asUInt();
    bomermansMove_.at(id).left_ = value["move"]["left"].asUInt();
    bomermansMove_.at(id).right_ = value["move"]["right"].asUInt();

    mutexClients_.unlock();

}

void GameMaster::onBombermanPlantBomb(Json::Value value) {

    unsigned id = value["id"].asUInt();

    mutexClients_.lock();

    bomermansPlant_.at(id) = true;

    mutexClients_.unlock();

}

/*
* Project: Bomberman
* Author: mc & md
* File:   bomb.cpp
*/

#include "bomb.hpp"

Bomb::Bomb(MapPosition map_position, double count_down_duration, double explosion_duration, const PWBomberman& bomberman)
: mapPosition_(map_position), countDownDuration_(count_down_duration), explosionDuration_(explosion_duration), bomberman_(bomberman) {
  bomberman_.lock()->decreaseBombsNumber();
  state_ = COUNT_DOWN;
  referenceTime_ = boost::chrono::steady_clock::now() + boost::chrono::duration_cast<boost::chrono::milliseconds>
                                                                (boost::chrono::duration<double>(countDownDuration_));
}

Bomb::~Bomb() {
}

void Bomb::dentonate() {

  bomberman_.lock()->increaseBombsNumber();

  referenceTime_ = boost::chrono::steady_clock::now() + boost::chrono::duration_cast<boost::chrono::milliseconds>
                                                                (boost::chrono::duration<double>(explosionDuration_));
  state_ = EXPLOSION;
}

Bomb::State Bomb::getState() {
  if (state_ == COUNT_DOWN) {
    if (referenceTime_ <= boost::chrono::steady_clock::now())
      state_ = START_EXPLOSION;
  } else if (state_ == EXPLOSION) {
    if (referenceTime_ <= boost::chrono::steady_clock::now())
      state_ = STOP_EXPLOSION;
  }

  return state_;
}

MapPosition Bomb::getPosition() const {
  return mapPosition_;
}

unsigned Bomb::getRange() const{
  return bomberman_.lock()->getBombRange();
}

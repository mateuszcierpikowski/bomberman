/*
 * Project: Bomberman
 * Author: mc & md
 * File:   factoryaddition.cpp
 */

#include "factoryaddition.hpp"


FactoryAddition::FactoryAddition() {

}

FactoryAddition& FactoryAddition::getInstance() {
    static FactoryAddition instance;
    return instance;
}

void FactoryAddition::registerAddition(AdditionAbs::AdditionType type, CreateAddition additon) {
    creators_[type] = additon;
}

PAddition FactoryAddition::create(AdditionAbs::AdditionType type) {
    Creators::const_iterator i = creators_.find(type);

    if (i != creators_.end())
        return PAddition((i->second) ());
    return nullptr;
}

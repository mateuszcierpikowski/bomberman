/*
 * Project: Bomberman
 * Author: mc & md
 * File:   fieldabs.cpp
 */

#include "fieldabs.hpp"

FieldAbs::FieldAbs(FieldData field_data, PAddition addition)
: fieldData_(field_data), addition_(addition), anyChange_(true) {

}

FieldAbs::~FieldAbs() { }

bool FieldAbs::checkMove() {
  if (fieldData_.type_ == Type::INDESTRUCTIBLE)
    return false;
  else if (fieldData_.subType_ == SubType::BARRICADE)
    return false;
  else if (bomb_.lock())
    return false;
  else
    return true;
}

bool FieldAbs::getFieldData(FieldData& field_data) {

  field_data = fieldData_;

  if(!anyChange_)
    return false;

  anyChange_ = false;

  return true;
}

bool FieldAbs::isBombPlanted() {

  if(bomb_.lock())
    return true;

  return false;
}

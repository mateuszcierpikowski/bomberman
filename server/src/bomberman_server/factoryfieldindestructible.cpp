/*
 * Project: Bomberman
 * Author: mc & md
 * File:   factoryfieldindestructible.cpp
 */

#include "factoryfieldindestructible.hpp"

FactoryFieldIndestructible::FactoryFieldIndestructible() {

}

FactoryFieldIndestructible::~FactoryFieldIndestructible() {

}

PField FactoryFieldIndestructible::createField(FieldAbs::FieldData type) {
    return PField(new FieldIndestructible(type));
}
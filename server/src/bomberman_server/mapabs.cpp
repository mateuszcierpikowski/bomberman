/*
 * Project: Bomberman
 * Author: mc & md
 * File:   mapabs.cpp
 */

#include "mapabs.hpp"

MapAbs::MapAbs(unsigned width, unsigned height, unsigned field_radius, double bomb_count_down_duration, double bomb_exlosion_duration)
  : width_(width), height_(height), fieldRadius_(field_radius), bombCountDownDuration_(bomb_count_down_duration),
    bombExlosionDuration_(bomb_exlosion_duration) { }

MapAbs::~MapAbs() { }

void MapAbs::bombermanMove(unsigned bomberman_id, Move move) {

  int move_x;
  int move_y;

  // determination of a horizontal move
  if(move.right_ != 0 && move.left_ == 0)
    move_x = 1;
  else if (move.right_ == 0 && move.left_ != 0)
    move_x = -1;
  else
    move_x = 0;

  // determination of vertical move
  if(move.down_ != 0 && move.up_ == 0)
    move_y = 1;
  else if (move.down_ == 0 && move.up_ != 0)
    move_y = -1;
  else
    move_y = 0;

  // if no move -> return
  if(move_x == 0 && move_y == 0)
    return;

  PBomberman bomberman = bombermanList_.at(bomberman_id);

  Position act_position = bomberman->getPosition();

  // check bomberman mode
  if (bomberman->getMode() == Bomberman::Mode::INVERT) {

    move_x *= -1;
    move_y *= -1;
  }

  // testing the possibility of moving to the next field horizontal if needed
  if(move_x != 0 && act_position.fieldPosition_.fieldX_ == 0) {

    if (move_x > 0) {

      if (!fieldList_[act_position.mapPosition_.mapY_][act_position.mapPosition_.mapX_ + 1]->checkMove())
        move_x = 0;
    }
    else {

      if (!fieldList_[act_position.mapPosition_.mapY_][act_position.mapPosition_.mapX_ - 1]->checkMove())
        move_x = 0;
    }
  }

  // testing the possibility of moving to the next field vertical if needed
  if(move_y != 0 && act_position.fieldPosition_.fieldY_ == 0) {

    if(move_y > 0) {
      if (!fieldList_[act_position.mapPosition_.mapY_ + 1][act_position.mapPosition_.mapX_]->checkMove())
        move_y = 0;
    }
    else {
      if (!fieldList_[act_position.mapPosition_.mapY_ - 1][act_position.mapPosition_.mapX_]->checkMove())
        move_y = 0;
    }
  }

  // if no move -> return
  if(move_x == 0 && move_y == 0)
    return;

  fieldList_.at(act_position.mapPosition_.mapY_).at(act_position.mapPosition_.mapX_)->deleteBomberman(bomberman);

  // if position on field 0,0
  if(act_position.fieldPosition_.fieldY_ == 0 && act_position.fieldPosition_.fieldX_ == 0) {

    if(move_x == 0 || move_y == 0) {

      act_position.fieldPosition_.fieldY_ += move_y;
      act_position.fieldPosition_.fieldX_ += move_x;
    }
  }
  // if vertical position on field 0
  else if (act_position.fieldPosition_.fieldY_ == 0) {

    // if horizontal move 0 - > add move
    if(move_x == 0) {
      if(act_position.fieldPosition_.fieldX_ < 0)
        move_x = 1;
      else
        move_x = -1;
    }

    // move
    act_position.fieldPosition_.fieldY_ += move_y;
    act_position.fieldPosition_.fieldX_ += move_x;

    // if horizontal position on field !0 - > vertical position on field 0
    if(abs(act_position.fieldPosition_.fieldX_) > 0)
      act_position.fieldPosition_.fieldY_ = 0;

    // if switch to the next field
    if (act_position.fieldPosition_.fieldX_ > fieldRadius_ - 1) {

      act_position.fieldPosition_.fieldX_ = -fieldRadius_;
      act_position.mapPosition_.mapX_ += 1;
    }

    // if switch to the previous field
    if (act_position.fieldPosition_.fieldX_ < -fieldRadius_) {

      act_position.fieldPosition_.fieldX_ = fieldRadius_ - 1;
      act_position.mapPosition_.mapX_ -= 1;
    }
  }
  // if horizontal position on field 0
  else if (act_position.fieldPosition_.fieldX_ == 0) {

    // if vertical move 0 - > add move
    if(move_y == 0) {
      if(act_position.fieldPosition_.fieldY_ < 0)
        move_y = 1;
      else
        move_y = -1;
    }

    // move
    act_position.fieldPosition_.fieldY_ += move_y;
    act_position.fieldPosition_.fieldX_ += move_x;

    // if vertical position on field !0 - > horizontal position on field 0
    if(abs(act_position.fieldPosition_.fieldY_) > 0)
      act_position.fieldPosition_.fieldX_ = 0;

    // if switch to the next field
    if (act_position.fieldPosition_.fieldY_ > fieldRadius_ - 1) {

      act_position.fieldPosition_.fieldY_ = -fieldRadius_;
      act_position.mapPosition_.mapY_ += 1;
    }

    // if switch to the previous field
    if (act_position.fieldPosition_.fieldY_ < -fieldRadius_) {

      act_position.fieldPosition_.fieldY_ = fieldRadius_ - 1;
      act_position.mapPosition_.mapY_ -= 1;
    }
  }

  // change position
  bomberman->setPosition(act_position);
  // add bomberman to field
  fieldList_.at(act_position.mapPosition_.mapY_).at(act_position.mapPosition_.mapX_)->addBomberman(bomberman);
}

void MapAbs::bombermanPlantBomb(unsigned bomberman_id) {

  PBomberman bomberman = bombermanList_.at(bomberman_id);

  // check if bomb remain
  if(!bomberman->anyBombRemain())
    return;

  PField actualField = fieldList_.at(bomberman->getPosition().mapPosition_.mapY_).at(bomberman->getPosition().mapPosition_.mapX_);

  // check if bomb is planted on actual field
  if(actualField->isBombPlanted())
    return;

  // create & add bomb
  PBomb bomb = PBomb(new Bomb(MapPosition(bomberman->getPosition().mapPosition_.mapX_,bomberman->getPosition().mapPosition_.mapY_),
                        bombCountDownDuration_, bombExlosionDuration_, bomberman));

  bombList_.push_back(bomb);
  actualField->addBomb(bomb);

}

VResult MapAbs::checkBombermans() {

  VResult result_list;
  Result result;
  int deadBombermanCounter = 0;

  // check bombermans lives
  for (auto it = bombermanList_.begin(); it != bombermanList_.end(); ++it) {
    result_list.push_back(Result::GAME_RUN);
    if (!(*it)->anyLiveRemain())
      ++deadBombermanCounter;
  }

  if (deadBombermanCounter == 0)
    return result_list;

  // if any bomberman dead change game status
  if (deadBombermanCounter == bombermanList_.size()) {

    for (unsigned i = 0; i < bombermanList_.size(); ++i)
      result_list.at(i) = Result::DRAW;
      return result_list;
  } else if (bombermanList_.size() - deadBombermanCounter == 1) {
    result = Result::VICTORY;
  } else {
    result = Result::GAME_RUN;
  }

  for (unsigned i = 0; i < bombermanList_.size(); ++i)
    if (bombermanList_.at(i)->anyLiveRemain())
      result_list.at(i) = result;
    else
      result_list.at(i) = Result::DEFEAT;

  return result_list;

}

void MapAbs::checkBombs() {

  // chck bomb states and start or stope explosion
  for (auto it = bombList_.begin(); it != bombList_.end(); ) {
    Bomb::State bomb_state = (*it)->getState();

    if (bomb_state == Bomb::STOP_EXPLOSION) {
      bombStopExplosion(*it);
      it = bombList_.erase(it);
  	}
  	else {
  		if (bomb_state == Bomb::START_EXPLOSION) {
  			(*it)->dentonate();
  			bombStartExplosion(*it);
  		}
  		++it;
  	}
  }

}

void MapAbs::bombStartExplosion(PBomb bomb, Direction direction_of_chain_reaction) {
  PBomb bomb_chain_reaction;

  // start explosion on actual field
  fieldList_.at(bomb->getPosition().mapY_).at(bomb->getPosition().mapX_)->startBombExplosion(bomb, bomb_chain_reaction);

  // propagate explosion in four directions (if bomb chain reaction do not propagate back)
  if (direction_of_chain_reaction != Direction::DOWN)
    propagateExplosion(bomb, 0, Direction::UP);

  if (direction_of_chain_reaction != Direction::UP)
    propagateExplosion(bomb, 0, Direction::DOWN);

  if (direction_of_chain_reaction != Direction::RIGHT)
    propagateExplosion(bomb, 0, Direction::LEFT);

  if (direction_of_chain_reaction != Direction::LEFT)
    propagateExplosion(bomb, 0, Direction::RIGHT);

}
void MapAbs::bombStopExplosion(PBomb bomb) {

  // stop explosiona on actuall field
  fieldList_.at(bomb->getPosition().mapY_).at(bomb->getPosition().mapX_)->stopBombExplosion(bomb);

  // decline explosion if four directions
  declineExplosion(bomb, 0, Direction::UP);
  declineExplosion(bomb, 0, Direction::DOWN);
  declineExplosion(bomb, 0, Direction::LEFT);
  declineExplosion(bomb, 0, Direction::RIGHT);

}
bool MapAbs::propagateExplosion(PBomb bomb, unsigned act_range, MapAbs::Direction direction) {

  ++act_range;

  // chceck bomb range
  if(act_range > bomb->getRange())
    return false;

  PBomb bomb_chain_reaction;
  bool next = false;

  // propagate explosion in selected direction
  if (direction == Direction::UP)
    next = fieldList_.at(bomb->getPosition().mapY_ - act_range).at(bomb->getPosition().mapX_)->startBombExplosion(bomb, bomb_chain_reaction);
  else if (direction == Direction::DOWN)
    next = fieldList_.at(bomb->getPosition().mapY_ + act_range).at(bomb->getPosition().mapX_)->startBombExplosion(bomb, bomb_chain_reaction);
  else if (direction == Direction::LEFT)
    next = fieldList_.at(bomb->getPosition().mapY_).at(bomb->getPosition().mapX_ - act_range)->startBombExplosion(bomb, bomb_chain_reaction);
  else if (direction == Direction::RIGHT)
    next = fieldList_.at(bomb->getPosition().mapY_).at(bomb->getPosition().mapX_ + act_range)->startBombExplosion(bomb, bomb_chain_reaction);

  // if true -> propagate to next field
  if (next) {
    return propagateExplosion(bomb, act_range, direction);
  }
  // if bomb chain reaction -> start explosion bomb chain reaction
  else if (bomb_chain_reaction) {

    bomb_chain_reaction->dentonate();
    bombStartExplosion(bomb_chain_reaction, direction);
    return false;
  }
  else {
    return false;
  }

}
bool MapAbs::declineExplosion(PBomb bomb, unsigned act_range, MapAbs::Direction direction) {

  ++act_range;

  // chceck bomb range
  if(act_range > bomb->getRange())
    return false;

  bool next = false;

  // decline explosion in selected direction
  if (direction == Direction::UP)
    next = fieldList_.at(bomb->getPosition().mapY_ - act_range).at(bomb->getPosition().mapX_)->stopBombExplosion(bomb);
  else if (direction == Direction::DOWN)
    next = fieldList_.at(bomb->getPosition().mapY_ + act_range).at(bomb->getPosition().mapX_)->stopBombExplosion(bomb);
  else if (direction == Direction::LEFT)
    next = fieldList_.at(bomb->getPosition().mapY_).at(bomb->getPosition().mapX_ - act_range)->stopBombExplosion(bomb);
  else if (direction == Direction::RIGHT)
    next = fieldList_.at(bomb->getPosition().mapY_).at(bomb->getPosition().mapX_ + act_range)->stopBombExplosion(bomb);

  // if true -> decline to next field
  if (next)
    return declineExplosion(bomb, act_range, direction);
  else
    return false;

}

VVFields MapAbs::getFieldList() {
  return fieldList_;
}

VBombermans MapAbs::getBombermanList() {
  return bombermanList_;
}

VBombs MapAbs::getBombList() {
  return bombList_;
}

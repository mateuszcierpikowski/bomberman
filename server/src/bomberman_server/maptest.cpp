/*
 * Project: Bomberman
 * Author: mc & md
 * File:   maptest.cpp
 */

#include "maptest.hpp"



 MapTest::MapTest(bool is_additional_bomberman)
     : MapAbs(11, 7, 10, 0.001, 0.001) {
     std::vector<std::vector<FieldAbs::FieldData>> table_fields_type;

     for (unsigned y = 0; y < 7; ++y) {
         std::vector<FieldAbs::FieldData> temp;
         for (unsigned x = 0; x < 11; ++x) {

           if ( x == 0 || y == 0 || x == 10 || y == 6 || (x%2 == 0 && y%2 == 0)) {
             temp.push_back(FieldAbs::FieldData(MapPosition(x, y), FieldAbs::Type::INDESTRUCTIBLE,
                                                FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::NONE));
           }
           else {
             temp.push_back(FieldAbs::FieldData(MapPosition(x, y), FieldAbs::Type::DESTRUCTIBLE,
                                                FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::NONE));
           }
         }
         table_fields_type.push_back(temp);
     }

     std::vector<int> x = { 1, 3, 3, 3, 4, 4, 5, 5, 6, 7, 7, 7, 8, 8, 9};
     std::vector<int> y = { 4, 1, 2, 3, 1, 3, 1, 5, 5, 1, 2, 4, 3, 5, 1};

     assert(x.size() == y.size());

     auto itx = x.begin();
     auto ity = y.begin();

     for(; itx != x.end(); ++itx, ++ity)
     {
       table_fields_type[*ity][*itx].changeSubType(FieldAbs::SubType::BARRICADE);
     }

     PFactoryField factory_destructible_field = PFactoryField(new FactoryFieldDestructible);
     PFactoryField factory_indestructible_field = PFactoryField(new FactoryFieldIndestructible);

     for (auto fields_type : table_fields_type) {
         VFields temp;
         for (auto field_type: fields_type) {
             if (field_type.type_ == FieldAbs::Type::DESTRUCTIBLE)
                 temp.push_back(factory_destructible_field->createField(field_type));
             else
                 temp.push_back(factory_indestructible_field->createField(field_type));
         }
         fieldList_.push_back(temp);
     }

     PBomberman bomberman = PBomberman(new Bomberman(Position(MapPosition(1, 1), FieldPosition(0, 0))));

     bombermanList_.push_back(bomberman);

     fieldList_.at(1).at(1)->addBomberman(bomberman);

     bomberman = PBomberman(new Bomberman(Position(MapPosition(1, 3), FieldPosition(0, 0))));

     bombermanList_.push_back(bomberman);

     fieldList_.at(3).at(1)->addBomberman(bomberman);

     if (is_additional_bomberman) {

         bomberman = PBomberman(new Bomberman(Position(MapPosition(9, 5), FieldPosition(0, 0))));

         bombermanList_.push_back(bomberman);

         fieldList_.at(5).at(9)->addBomberman(bomberman);

         bomberman = PBomberman(new Bomberman(Position(MapPosition(9, 3), FieldPosition(0, 0))));

         bombermanList_.push_back(bomberman);

         fieldList_.at(3).at(9)->addBomberman(bomberman);
     }

 }

 MapTest::~MapTest() { }

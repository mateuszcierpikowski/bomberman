/*
 * Project: Bomberman
 * Author: mc & md
 * File:   bomberman.cpp
 */

#include "bomberman.hpp"

Bomberman::Bomberman(Position position) : position_(position), mode_(NORMAL), lives_(1), bombsNumber_(1), bombsRange_(1),
                                        anyAttributeChange_(true) { }

Bomberman::~Bomberman() { }

void Bomberman::setPosition(Position position) {
    position_ = position;
}

Position Bomberman::getPosition() const {
    return position_;
}

void Bomberman::setInvertMode(double duration) {
  invertEndTime_ = boost::chrono::steady_clock::now() + boost::chrono::duration_cast<boost::chrono::milliseconds>
                                                                (boost::chrono::duration<double>(duration));
  mode_ = Bomberman::INVERT;
}

Bomberman::Mode Bomberman::getMode() {
  if(mode_ == INVERT)
    if(invertEndTime_ <= boost::chrono::steady_clock::now())
      mode_ = NORMAL;

  return mode_;
}

void Bomberman::increaseLives() {
    anyAttributeChange_ = true;
    ++lives_;
}

void Bomberman::decreaseLives() {
    if (lives_ > 0) {
        anyAttributeChange_ = true;
        --lives_;
    }
}

bool Bomberman::anyLiveRemain() const {
  if(lives_)
    return true;

  return false;
}

unsigned Bomberman::getLives() const {
    return lives_;
}

void Bomberman::increaseBombsNumber() {
    anyAttributeChange_ = true;
    ++bombsNumber_;
}

void Bomberman::decreaseBombsNumber() {
    anyAttributeChange_ = true;
    --bombsNumber_;
}

bool Bomberman::anyBombRemain() const {
  if(bombsNumber_)
    return true;

  return false;
}

unsigned Bomberman::getBombsNumber() const {
    return bombsNumber_;
}

void Bomberman::increaseBombRange() {
    anyAttributeChange_ = true;
    ++bombsRange_;
}

unsigned Bomberman::getBombRange() const {
    return bombsRange_;
}

bool Bomberman::anyAttributeChange() {
    if (anyAttributeChange_) {
        anyAttributeChange_ = false;
        return true;
    }

    return false;
}
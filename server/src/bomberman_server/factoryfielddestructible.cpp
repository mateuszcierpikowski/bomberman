/*
 * Project: Bomberman
 * Author: mc & md
 * File:   factoryfielddestructible.cpp
 */

#include "factoryfielddestructible.hpp"

AdditionAbs* CreateAdditionExtraBomb() {
    return new AdditionPositiveExtraBomb();
}

AdditionAbs* CreateAdditionExtraRange() {
    return new AdditionPositiveExtraRange();
}

AdditionAbs* CreateAdditionInvertMode() {
    return new AdditionNegativeInvertMode(FactoryFieldDestructible::TIME_OF_INVERT_STEERING_MODE);
}

FactoryFieldDestructible::FactoryFieldDestructible() {
    FactoryAddition& factory_addition = FactoryAddition::getInstance();

    factory_addition.registerAddition(AdditionAbs::AdditionType::EXTRA_BOMB, CreateAdditionExtraBomb);
    factory_addition.registerAddition(AdditionAbs::AdditionType::EXTRA_RANGE, CreateAdditionExtraRange);
    factory_addition.registerAddition(AdditionAbs::AdditionType::INVERT_MODE, CreateAdditionInvertMode);
}

FactoryFieldDestructible::~FactoryFieldDestructible() {

}

PField FactoryFieldDestructible::createField(FieldAbs::FieldData type) {
    return PField(new FieldDestructible(type, FactoryAddition::getInstance().create(type.additionType_)));
}

const double FactoryFieldDestructible::TIME_OF_INVERT_STEERING_MODE = 10;
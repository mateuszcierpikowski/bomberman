/*
 * Project: Bomberman
 * Author: mc & md
 * File:   fieldindestructible.cpp
 */

#include "fieldindestructible.hpp"

FieldIndestructible::FieldIndestructible(FieldData field_data)
: FieldAbs(field_data, nullptr) { }

FieldIndestructible::~FieldIndestructible() { }

void FieldIndestructible::addBomberman(const PBomberman & bomberman) { }

void FieldIndestructible::deleteBomberman(const PBomberman & bomberman) { }

void FieldIndestructible::addBomb(const PBomb & bomb) { }

bool FieldIndestructible::startBombExplosion(const PBomb & bomb, PBomb & bomb_chain_reaction) {
    return false;
}

bool FieldIndestructible::stopBombExplosion(const PBomb & bomb) {
    return false;
}

/*
* Project: Bomberman
* Author: mc & md
* File:   fielddestructible.cpp
*/

#include "fielddestructible.hpp"
#include <iostream>
#include <assert.h>

FieldDestructible::FieldDestructible(FieldData field_data, PAddition addition = nullptr)
: FieldAbs(field_data, addition) { }

FieldDestructible::~FieldDestructible() { }

void FieldDestructible::addBomberman(const PBomberman& bomberman) {
  // if explosion on field -> decrese bomberman lives
  if (fieldData_.subType_ == SubType::EXPLOSION)
    bomberman->decreaseLives();

  // if addition on field - > execute addition
  if (addition_) {
    addition_->execute(bomberman);
    addition_.reset();
    fieldData_.deleteAddition();
    anyChange_ = true;
  }

  bombermansList_.push_back(bomberman);
}

void FieldDestructible::deleteBomberman(const PBomberman & bomberman) {
  // deelete selected bomberman
  for (auto it = bombermansList_.begin(); it != bombermansList_.end(); ++it)
    if (*it == bomberman) {
      bombermansList_.erase(it);
      return;
    }
}

void FieldDestructible::addBomb(const PBomb & bomb) {
  // if bomb is not yet planted
  if(!bomb_.lock()) {
    fieldData_.subType_ = SubType::PLANTED;
    bomb_ = bomb;
    anyChange_ = true;
  }
}

bool FieldDestructible::startBombExplosion(const PBomb & bomb, PBomb & bomb_chain_reaction) {
  bombDetonated_ = bomb;
  
  anyChange_ = true;

  // if SubType::BARRICADE
  if (fieldData_.subType_ == SubType::BARRICADE) {
    fieldData_.changeSubType(SubType::EXPLOSION);
    return false;
  }

  fieldData_.changeSubType(SubType::EXPLOSION);

  // if bomb on field
  if (bomb_.lock())
    // if exploding bomb is bomb on field -> delete bomb
    if (bomb_.lock() == bomb)
      bomb_.reset();
    // if exploding bomb is not bomb on field -> add bomb chain reaction
    else {
      bomb_chain_reaction = bomb_.lock();
      return false;
    }

  // kill bombrmans
  for (PBomberman bomberman: bombermansList_)
    bomberman->decreaseLives();

  // delete additions
  addition_.reset();
  fieldData_.deleteAddition();

  return true;
}

bool FieldDestructible::stopBombExplosion(const PBomb & bomb) {

  if (fieldData_.subType_ == SubType::BARRICADE)
    return false;

  if (bombDetonated_.lock() == bomb) {
    fieldData_.changeSubType(SubType::EMPTY);
    anyChange_ = true;
  }

  return true;
}

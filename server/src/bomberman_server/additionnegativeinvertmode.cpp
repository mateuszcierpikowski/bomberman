/*
 * Project: Bomberman
 * Author: mc & md
 * File:   additionnegativeinvertmode.cpp
 */

#include "additionnegativeinvertmode.hpp"

AdditionNegativeInvertMode::AdditionNegativeInvertMode(double duration) : duration_(duration) { }

AdditionNegativeInvertMode::~AdditionNegativeInvertMode() { }

void AdditionNegativeInvertMode::execute (const PBomberman& bomberman) {
    bomberman->setInvertMode(duration_);
}

AdditionAbs::AdditionType AdditionNegativeInvertMode::getType() {
  return AdditionType::INVERT_MODE;
}

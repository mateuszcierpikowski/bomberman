/*
 * Project: Bomberman
 * Author: mc & md
 * File:   mapnormal.cpp
 */

#include "mapnormal.hpp"

MapNormal::MapNormal(unsigned width, unsigned height, unsigned field_radius, unsigned number_of_positive_addition
        , unsigned number_of_negative_addition, unsigned percent_of_barricaded_field, double bomb_count_down_duration,
                     double bomb_exlosion_duration)
    : MapAbs(width, height, field_radius, bomb_count_down_duration, bomb_exlosion_duration),
      numberOfPositiveAddition_(number_of_positive_addition), numberOfNegativeAddition_(number_of_negative_addition),
      percentOfBarricadedField_(percent_of_barricaded_field)
{
    std::vector<MapPosition> fields_position;
    std::vector<std::vector<FieldAbs::FieldData>> table_fields_type;

    generator_.seed(boost::posix_time::second_clock::local_time().time_of_day().seconds());

    initFieldsTypeTable(table_fields_type);
    initFieldsPosition(fields_position);

    int number_of_barricaded_field = fields_position.size() * percentOfBarricadedField_ / 100;

    number_of_barricaded_field -= numberOfNegativeAddition_;
    number_of_barricaded_field -= numberOfPositiveAddition_;

    changeAllTypeFieldsToDestructible(fields_position, table_fields_type);

    drawPositiveAddition(number_of_positive_addition, fields_position, table_fields_type);
    drawNegativeAddition(number_of_negative_addition, fields_position, table_fields_type);
    drawBarricadedPosition(number_of_barricaded_field, fields_position, table_fields_type);

    createFieldList(table_fields_type);

    initBombermansPosition();

}

MapNormal::~MapNormal() {

}

void MapNormal::drawPositiveAddition(int number_of_positive_addition, VMapPosition& fields_position,
                                     VVFieldData& table_fields_type) {

    while (number_of_positive_addition > 0) {

        int rand_position = randomInt(fields_position.size() - 1);

        table_fields_type[fields_position[rand_position].mapY_][fields_position[rand_position].mapX_].changeSubType(FieldAbs::SubType::BARRICADE);

        int rand_addition = randomInt(NUMBER_OF_POSITIVE_ADDITION - 1);

        switch (rand_addition) {
            case 0 :
                table_fields_type[fields_position[rand_position].mapY_][fields_position[rand_position].mapX_].changeAddition(AdditionAbs::AdditionType::EXTRA_BOMB);
                break;
            case 1 :
                table_fields_type[fields_position[rand_position].mapY_][fields_position[rand_position].mapX_].changeAddition(AdditionAbs::AdditionType::EXTRA_RANGE);
                break;
        }

        fields_position.erase(fields_position.begin() + rand_position);

        --number_of_positive_addition;
    }

}

void MapNormal::drawNegativeAddition(int number_of_negative_addition, VMapPosition& fields_position,
                                     VVFieldData& table_fields_type) {

    while (number_of_negative_addition > 0) {

        int rand_position = randomInt(fields_position.size() - 1);

        table_fields_type[fields_position[rand_position].mapY_][fields_position[rand_position].mapX_].changeSubType(FieldAbs::SubType::BARRICADE);

        table_fields_type[fields_position[rand_position].mapY_][fields_position[rand_position].mapX_].changeAddition(AdditionAbs::AdditionType::INVERT_MODE);

        fields_position.erase(fields_position.begin() + rand_position);

        --number_of_negative_addition;
    }
}

void MapNormal::drawBarricadedPosition(int number_of_barricaded_field, VMapPosition& fields_position,
                                       VVFieldData& table_fields_type) {

    while (number_of_barricaded_field > 0) {

        int rand_position = randomInt(fields_position.size() - 1);

        table_fields_type[fields_position[rand_position].mapY_][fields_position[rand_position].mapX_].changeSubType(FieldAbs::SubType::BARRICADE);

        fields_position.erase(fields_position.begin() + rand_position);

        --number_of_barricaded_field;
    }
}

void MapNormal::createFieldList(VVFieldData &table_fields_type) {

    PFactoryField factory_destructible_field = PFactoryField(new FactoryFieldDestructible);
    PFactoryField factory_indestructible_field = PFactoryField(new FactoryFieldIndestructible);

    for (auto fields_type : table_fields_type) {
        VFields temp;
        for (auto field_type: fields_type) {
            if (field_type.type_ == FieldAbs::Type::DESTRUCTIBLE)
                temp.push_back(factory_destructible_field->createField(field_type));
            else
                temp.push_back(factory_indestructible_field->createField(field_type));
        }
        fieldList_.push_back(temp);
    }

}

void MapNormal::initFieldsTypeTable(VVFieldData& table_fields_type) {

    for (unsigned y = 0; y < height_; ++y) {
        std::vector<FieldAbs::FieldData> temp;
        for (unsigned x = 0; x < width_; ++x)
            temp.push_back(FieldAbs::FieldData(MapPosition(x, y), FieldAbs::Type::INDESTRUCTIBLE,
                                               FieldAbs::SubType::EMPTY, AdditionAbs::AdditionType::NONE));
        table_fields_type.push_back(temp);
    }

    // change empty for init bomberman's position
    table_fields_type[1][1].changeType(FieldAbs::Type::DESTRUCTIBLE);
    table_fields_type[2][1].changeType(FieldAbs::Type::DESTRUCTIBLE);
    table_fields_type[1][2].changeType(FieldAbs::Type::DESTRUCTIBLE);

    table_fields_type[height_ - 2][width_ - 2].changeType(FieldAbs::Type::DESTRUCTIBLE);
    table_fields_type[height_ - 3][width_ - 2].changeType(FieldAbs::Type::DESTRUCTIBLE);
    table_fields_type[height_ - 2][width_ - 3].changeType(FieldAbs::Type::DESTRUCTIBLE);

}

void MapNormal::initFieldsPosition(VMapPosition &fields_position) {

    for (unsigned y = 1; y < height_ - 1; ++y) {

        unsigned x = 1;
        unsigned max = width_ - 1;

        if (y == 1 || y == 2)
            x = 3;

        if (y == height_ - 2 || y == height_ - 3)
            max = width_ - 3;

        for (; x < max; ++x) {
            if (y % 2 || x % 2)
                fields_position.push_back(MapPosition(x, y));
        }
    }
}

void MapNormal::changeAllTypeFieldsToDestructible(VMapPosition& fields_position, VVFieldData& table_fields_type) {

    for (auto field: fields_position) {
        table_fields_type[field.mapY_][field.mapX_].changeType(FieldAbs::Type::DESTRUCTIBLE);
    }
}

int MapNormal::randomInt(int max_range) {
    boost::random::uniform_int_distribution<> dist(0, max_range);
    return dist(generator_);
}

void MapNormal::initBombermansPosition() {

    PBomberman bomberman = PBomberman(new Bomberman(Position(MapPosition(1, 1), FieldPosition(0, 0))));

    bombermanList_.push_back(bomberman);

    fieldList_.at(1).at(1)->addBomberman(bomberman);

    bomberman = PBomberman(new Bomberman(Position(MapPosition(width_ - 2, height_ - 2), FieldPosition(0, 0))));

    bombermanList_.push_back(bomberman);

    fieldList_.at(height_ - 2).at(width_ - 2)->addBomberman(bomberman);

}

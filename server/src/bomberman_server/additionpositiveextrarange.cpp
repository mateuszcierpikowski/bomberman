/*
 * Project: Bomberman
 * Author: mc & md
 * File:   additionpositiveextrarange.cpp
 */

#include "additionpositiveextrarange.hpp"

AdditionPositiveExtraRange::AdditionPositiveExtraRange() {
}

AdditionPositiveExtraRange::~AdditionPositiveExtraRange() {
}

void AdditionPositiveExtraRange::execute (const PBomberman& bomberman) {
    bomberman->increaseBombRange();
}

AdditionAbs::AdditionType AdditionPositiveExtraRange::getType() {
  return AdditionType::EXTRA_RANGE;
}

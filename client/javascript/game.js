var canvas;
var ctx;

// game area size
var width;
var height;

// map size
var mapWidth;
var mapHeight;

// field size and steps configuration
var sizeField = 41;
var fieldSteps = (sizeField - 1) / (2*6);

// bomberman and explosion size
var sizeBomb = 51;
var sizeExplosion = 43;

// web socket configuration
var webSocket;
var portNumber = 8383;
var ip = "localhost"

// bomb, bomberman and map info
var actualMapInfo = [];
var actualBombermanPosition;
var actualBombPosition;

// game state variables
var timerCountDown;
var timeOfGame = 0;
var gameStarted = false;

// necessary images declaration
var indestructibleFieldImage = new Image();
var brickImage = new Image();

var extraBombImage = new Image();
var extraRangeImage = new Image();
var invertModeImage = new Image();

indestructibleFieldImage.src = 'images/indestructible.png';
brickImage.src = 'images/brick.png';
extraBombImage.src = 'images/addition_bomb.png';
extraRangeImage.src = 'images/addition_range.png';
invertModeImage.src = 'images/addition_invert.png';

var explosionImages = new Array(5);

for (var i = 0; i < 5; ++i)
  explosionImages[i] = new Image();

explosionImages[0].src = 'images/explosion1.png';
explosionImages[1].src = 'images/explosion2.png';
explosionImages[2].src = 'images/explosion3.png';
explosionImages[3].src = 'images/explosion4.png';
explosionImages[4].src = 'images/explosion5.png';


var bombImages = new Array(2);

for (var i = 0; i < 2; ++i)
  bombImages[i] = new Image();

bombImages[0].src = 'images/bomb1.png';
bombImages[1].src = 'images/bomb2.png';

var bombermanImages = new Array(2);

for (var i = 0; i < 2; ++i){
  bombermanImages[i] = new Array(2);
  for (var j = 0; j < 2; ++j)
    bombermanImages[i][j] = new Image();
}

bombermanImages[0][0].src = 'images/bomberman1a.png';
bombermanImages[0][1].src = 'images/bomberman1b.png';

bombermanImages[1][0].src = 'images/bomberman2a.png';
bombermanImages[1][1].src = 'images/bomberman2b.png';

// variable to view animations
var explosionCounterAnimation = 0;
var bombermanCounterAnimation = 0;
var bombCounterAnimation = 0;
var explosionIncrease = true;
var timerAnimation = null;

// this variable specifies bomberman move
var move = {
  up: 0,
  down: 0,
  left: 0,
  right: 0
}

// this variable is sending when bomberman move
var msg = {
  type: "bombermanMove",
  move: move,
  id:   0,
  date: Date.now()
}

// this variable is sending when bomberman plant the bomb
var plant = {
  type: "bombermanPlantBomb",
  id:   0,
  date: Date.now()
}

// this enum specifies field info
var fieldsInfo = {
  type: 0,
  subtype: 0,
  addition: 0
}

// this enum specifies the field type
var Type = {
  INDESTRUCTIBLE : 1, DESTRUCTIBLE : 2
};

// this enum specifies the field subtype
var SubType = {
  EMPTY : 10, BARRICADE : 11, EXPLOSION : 12, PLANTED : 13
};

// this enum specifies the addition type
var AdditionType = {
    NONE : 100, EXTRA_BOMB : 101, EXTRA_RANGE : 102, INVERT_MODE : 103
};
// this enum specifies the game result
var Result = {
    VICTORY : 0, DRAW : 1, DEFEAT : 2, GAME_RUN : 3
};

// this method is called when document is ready
$(document).ready(function() {

  // set the size of canvas
  canvas = document.getElementById('can');
  ctx = canvas.getContext('2d');
  width = window.width;
  height = window.height;

  fitToContainer(canvas);

  // this function animates buttons
  $('.buttonMenu')
  .mouseover(function() {
      $(this).stop().animate({width:300}, 300);
  })
  .mouseout(function() {
      $(this).stop().animate({width:200}, 300);
  });

  // this function animates buttons
  $('.undo')
  .mouseover(function() {
      $(this).stop().animate({width:100}, 300);
  })
  .mouseout(function() {
      $(this).stop().animate({width:60}, 300);
  });


  // this function animates buttons and changes the view
  $('#start').click(function(){

    if (gameStarted)
      return;

    $("#startMenuWrapper").animate({opacity: 0 }, 1000);
    $("#bombermanTitle").animate({opacity: 0 }, 1000, function()
    {
      $("#gameAreaWrapper").animate({opacity: 1 }, 1000);
      $("#gameArea").animate({opacity: 0.1 }, 1000);
      $("#gameInfo").animate({opacity: 0.2 }, 1000, function() {
            // create new web socket
            webSocket = new WebSocket("ws://" + ip + ":" + portNumber + "/bombermanGame");
            webSocket.onmessage = onDataReceived;
      });
    });

  });

  // this function animates buttons and changes the view
  $('#undo').click(function(){

    $("#gameAreaWrapper").animate({opacity: 0 }, 1000);
    $("#gameArea").animate({opacity: 0 }, 1000);
    $("#gameInfo").animate({opacity: 0 }, 1000, function() {
      $("#startMenuWrapper").animate({opacity: 1 }, 1000);
      $("#bombermanTitle").animate({opacity: 1 }, 1000);
      stopGame();
    });
  });

})

document.onkeyup = onKeyUp;
document.onkeydown = onKeyDown;

// this method is called, when user pressed the key
function onKeyDown(e) {

  // up arrow
  if (e.keyCode == '38') {
    msg.move.up = 1;
  } // down arrow
  else if (e.keyCode == '40') {
    msg.move.down = 1;
  } // left arrow
  else if (e.keyCode == '37') {
    msg.move.left = 1;
  } // right arrow
  else if (e.keyCode == '39') {
    msg.move.right = 1;
  } // enter
  else if (e.keyCode == '32') {
    if(gameStarted)
      webSocket.send(JSON.stringify(plant));
  }

  // send the new position of the bomberman
  sendPosition();
}

// this method is called, when user released the key
function onKeyUp(e) {

  // up arrow
  if (e.keyCode == '38') {
    msg.move.up = 0;
  } // down arrow
  else if (e.keyCode == '40') {
    msg.move.down = 0;
  } // left arrow
  else if (e.keyCode == '37') {
    msg.move.left = 0;
  } // right arrow
  else if (e.keyCode == '39') {
    msg.move.right = 0;
  }
  // send the new position of the bomberman
  sendPosition();
}


// this method draws map and fields
function drawMap(map) {

  if(map == null)
    return;

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  for (var i = 0; i < actualMapInfo.length; ++i) {
    if (actualMapInfo[i].type == Type.INDESTRUCTIBLE)
      ctx.drawImage(indestructibleFieldImage, i%mapWidth*sizeField, parseInt(i/mapWidth)*sizeField, sizeField ,sizeField);
    else if (actualMapInfo[i].subtype == SubType.BARRICADE)
      ctx.drawImage(brickImage, i%mapWidth*sizeField, parseInt(i/mapWidth)*sizeField, sizeField ,sizeField);
    else if (actualMapInfo[i].subtype == SubType.EMPTY) {
      if (actualMapInfo[i].addition == AdditionType.EXTRA_BOMB)
        ctx.drawImage(extraBombImage, i%mapWidth*sizeField, parseInt(i/mapWidth)*sizeField, sizeField ,sizeField);
      else if (actualMapInfo[i].addition == AdditionType.EXTRA_RANGE)
        ctx.drawImage(extraRangeImage, i%mapWidth*sizeField, parseInt(i/mapWidth)*sizeField, sizeField ,sizeField);
      else if (actualMapInfo[i].addition == AdditionType.INVERT_MODE)
        ctx.drawImage(invertModeImage, i%mapWidth*sizeField, parseInt(i/mapWidth)*sizeField, sizeField ,sizeField);
    }
    else if (actualMapInfo[i].subtype == SubType.EXPLOSION) {
      ctx.drawImage(explosionImages[explosionCounterAnimation], i%mapWidth*sizeField - 1, parseInt(i/mapWidth)*sizeField - 1, sizeExplosion, sizeExplosion);
    }
    else if (actualMapInfo[i].subtype == SubType.PLANTED) {
      ctx.drawImage(bombImages[bombCounterAnimation], i%mapWidth*sizeField - 5, parseInt(i/mapWidth)*sizeField - 5, sizeBomb ,sizeBomb);
    }
  }

  ctx = canvas.getContext('2d');
}

// this method draws bomberman on canvas
function drawBomberman(bomberman) {

  if(bomberman == null)
    return;

  for (var i = 0; i < bomberman.length; i++) {
    ctx.drawImage(bombermanImages[i][bombermanCounterAnimation], bomberman[i]["mapX"]*sizeField + fieldSteps*bomberman[i]["fieldX"], bomberman[i]["mapY"]*sizeField + fieldSteps*bomberman[i]["fieldY"], sizeField ,sizeField);
  }

  ctx = canvas.getContext('2d');
}

// this method is called, when new message received
function onDataReceived(received) {

    // parse json message
    var data = JSON.parse(received.data);

    switch (data["type"]) {
      case "mapInfo": // update map info and clear old map
          mapWidth = data["data"]["width"];
          mapHeight = data["data"]["height"];
          for (var i = 0; i < mapWidth*mapHeight; ++i) {
              actualMapInfo.push(jQuery.extend(true, {}, fieldsInfo));
          }
          return;
          break;
      case "fieldsInfo": // update fields type
          if (data["data"] == null)
              break;
          for (var i = 0; i < data["data"].length; ++i) {
              var fieldInfo = data["data"][i];
              var index = fieldInfo["mapX"] + fieldInfo["mapY"] * mapWidth;
              actualMapInfo[index].type = fieldInfo["type"];
              actualMapInfo[index].subtype = fieldInfo["subtype"];
              actualMapInfo[index].addition = fieldInfo["addition"];
          }
          break;
      case "bombermansPosition": // update bombermans position
          actualBombermanPosition = data["data"];
          break;
      case "bombsPosition": // update bombs
          actualBombPosition = data["data"];
          break;
      case "playerId": // received player id
          playerId = data["data"];
          msg.id = playerId;
          plant.id = playerId;
          break;
      case "bombermanStats": // update bomberman stats
          var stats = data["data"];
          document.getElementById("livesNumber").textContent = stats.lives.toString();
          document.getElementById("bombsNumber").textContent = stats.bombs.toString();
          document.getElementById("rangeBombNumber").textContent = stats.range.toString();
          break;
      case "startGame": // start the game
          countDown(3);
          timerAnimation = setInterval(animation, 400);
          break;
      case "endGame": // end the game

          if (!gameStarted)
            break;

          gameStarted = false;

          // stop the game
          stopGame();

          // show the game result
          if (data["data"] == Result.VICTORY)
            endGame("WYGRANA");
          else if(data["data"] == Result.DRAW)
            endGame("REMIS");
          else if(data["data"] == Result.DEFEAT)
            endGame("PRZEGRANA");
          break;
    }

    // update the view
    drawMap(actualMapInfo);
    drawBomberman(actualBombermanPosition);

}

// this method fits canvas to game area
function fitToContainer(canvas){
  canvas.style.width = '100%';
  canvas.style.height = '100%';
  canvas.width  = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
}

// this method counts down to game start
function countDown(number) {

  // if count down number is 0 then change text to "Start" and animate it
  // else change count down number
  if (number == 0) {
    document.getElementById("waitingForPlayers").textContent = "Start";
    $("#gameArea").animate({opacity: 1 }, 1000);
    $("#gameInfo").animate({opacity: 1 }, 1000);
    timerCountDown = setInterval(setGameTime, 1000);
  }
  else
    document.getElementById("waitingForPlayers").textContent = number.toString() + "..";

  // animate text
  $("#waitingForPlayers").animate({fontSize: 120}, 500, function()
  {
      $("#waitingForPlayers").animate({fontSize: 70}, 500, function()
      {
          if (number == 0) {
            return;
          } else if (number == 1) {
            gameStarted = true;
          }

          number--;
          countDown(number);
      });
  });
}

// this method changes time of the game
function setGameTime() {
    document.getElementById("timeNumber").textContent = timeOfGame.toString() + " s";
    timeOfGame++;
}

// this method animates a game result when the game ends
function endGame(message) {

  document.getElementById("waitingForPlayers").textContent = message.toString();

  $("#gameArea").animate({opacity: 0.1 }, 1000);

  $("#waitingForPlayers").animate({fontSize: 120}, 1500, function()
  {
      $("#waitingForPlayers").animate({fontSize: 70}, 1500);
  });
}

// this method sends a bomberman position to server
function sendPosition() {
  if(!gameStarted)
    return;

  webSocket.send(JSON.stringify(msg));
}

// this method update bomberman move when user presses up key
function onKeyUpPress() {
  msg.move.up++;
}

// this method update bomberman move when user presses down key
function onKeyDownPress() {
  msg.move.down++
}

// this method update bomberman move when user presses left key
function onKeyLeftPress() {
  msg.move.left++;
}

// this method update bomberman move when user presses right key
function onKeyRightPress() {
  msg.move.right++;
}

// this method changes bomberman stats and other values to default and clears all timers
function stopGame() {
  webSocket.close();
  clearTimeout(timerCountDown);
  clearTimeout(timerAnimation);

  timeOfGame = 0;
  gameStarted = false;

  document.getElementById("waitingForPlayers").textContent = "Oczekiwanie na graczy";
  document.getElementById("livesNumber").textContent = "...";
  document.getElementById("bombsNumber").textContent = "...";
  document.getElementById("rangeBombNumber").textContent = "...";
  document.getElementById("timeNumber").textContent = "...";
}

// this method animates explosion, bomb and bomberman view
function animation() {

  if (explosionCounterAnimation == 4){
    explosionIncrease = false;
  }
  else if(explosionCounterAnimation == 0){
    explosionIncrease = true;
  }

  if (explosionIncrease)
    explosionCounterAnimation++;
  else
    explosionCounterAnimation--;

  if (bombermanCounterAnimation < 1)
    bombermanCounterAnimation++;
  else
    bombermanCounterAnimation = 0;

  if (bombCounterAnimation < 1)
    bombCounterAnimation++;
  else
    bombCounterAnimation = 0;
}
